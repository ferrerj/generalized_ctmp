#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <stdio.h> 
#include <signal.h>
using namespace std;


struct Literal {
    bool negated;
    string atom;

    static Literal make_atom(const string &atom) {
        Literal result;
        result.negated = false;
        result.atom = atom;
        return result;
    }

    bool operator<(const Literal &other) const {
        return make_pair(negated, atom) < make_pair(other.negated, other.atom);
    }
};


struct Conjunction {
    vector<Literal> literals;

    Conjunction sorted() const {
        Conjunction result(*this);
        sort(result.literals.begin(), result.literals.end());
        return result;
    }

    bool operator<(const Conjunction &other) const {
        return literals < other.literals;
    }
};


struct Disjunction {
    vector<Literal> literals;

    Disjunction sorted() const {
        Disjunction result(*this);
        sort(result.literals.begin(), result.literals.end());
        return result;
    }

    bool operator<(const Disjunction &other) const {
        return literals < other.literals;
    }
};


struct CNF {
    vector<Disjunction> clauses;
};


struct Effect {
    Conjunction conditions;
    Literal effect;
};


struct Action {
    string name;
    Conjunction precondition;
    vector<Effect> effects;
};


struct Axiom {
    CNF body;
    Literal head; // always positive

    static Axiom make_simple_axiom(const Literal &body, const Literal &head) {
        // Create an axiom of the form body -> head where body is a literal.
        Axiom result;
        Disjunction unit_clause;
        unit_clause.literals.push_back(body);
        result.body = CNF();
        result.body.clauses.push_back(unit_clause);
        result.head = head;
        return result;
    }
};


class Matcher;


static void error(const string &msg);
static void convert(const char *domain_filename,
                    const char *problem_filename);
static set<string> get_derived_variables(
    const set<string> &init,
    const vector<Action> &actions,
    const vector<Axiom> &axioms);
static void compile_away_cnf(
    set<string> &atoms,
    set<string> &derived_vars,
    vector<Axiom> &axioms);
static void dump_sas_file(
    const set<string> &atoms,
    const set<string> &derived_vars,
    const set<string> &init,
    const Conjunction &goal,
    const vector<Action> &actions,
    const vector<Axiom> &axioms);
static pair<int, int> convert_literal(const Literal &literal,
                                      const map<string, int> &atom_to_varno);
static vector<pair<int, int> > convert_conjunction(
    const Conjunction &conj, const map<string, int> &atom_to_varno);
static void tokenize(const char *filename, vector<string> &result);
static Literal parse_atom(Matcher &matcher, const set<string> &atoms);
static Literal parse_literal(Matcher &matcher, const set<string> &atoms);
static Conjunction parse_conjunction(Matcher &matcher,
                                     const set<string> &atoms);
static Disjunction parse_disjunction(Matcher &matcher,
                                     const set<string> &atoms);
static CNF parse_cnf(Matcher &matcher, const set<string> &atoms);
static Action parse_action(Matcher &matcher, const set<string> &atoms);
static Axiom parse_derived(Matcher &matcher, const set<string> &atoms);


class Matcher {
    vector<string> tokens;
    size_t position;
public:
    explicit Matcher(const vector<string> &tokens_)
        : tokens(tokens_), position(0) {
    }

    ~Matcher() {
    }

    string peek(size_t offset = 0) {
        if (position + offset >= tokens.size())
            fail("unexpected end of file");
        return tokens[position + offset];
    }

    string last() {
        if (position == 0)
            return "";
        else
            return tokens[position - 1];
    }

    string get() {
        string result = peek();
        ++position;
        return result;
    }

    void skip() {
        get();
    }

    void match(const string &token) {
        if (get() != token)
            fail("mismatch: expected '" + token + "', got '" + last() + "'");
    }

    void match_end() {
        if (position != tokens.size())
            fail("expected end of file");
    }

    void fail(const string &msg) const {
        error(msg);
    }
};


class AuxiliaryAxiomGenerator {
    map<Disjunction, string> disjunction_to_atom;

    string get_next_atom() const {
        int id = disjunction_to_atom.size();
        ostringstream atom_name;
        atom_name << "axiom@" << id;
        return atom_name.str();
    }
public:
    AuxiliaryAxiomGenerator() {
    }

    ~AuxiliaryAxiomGenerator() {
    }

    string get_auxiliary_atom_for_disjunction(const Disjunction &disj) {
        return disjunction_to_atom.insert(
            make_pair(disj.sorted(), get_next_atom())
            ).first->second;
    }

    vector<string> get_auxiliary_atoms() const {
        vector<string> result;
        map<Disjunction, string>::const_iterator iter;
        for (iter = disjunction_to_atom.begin();
             iter != disjunction_to_atom.end(); ++iter)
            result.push_back(iter->second);
        return result;
    }

    vector<Axiom> get_auxiliary_axioms() const {
        vector<Axiom> result;
        map<Disjunction, string>::const_iterator iter;
        for (iter = disjunction_to_atom.begin();
             iter != disjunction_to_atom.end(); ++iter) {
            const Disjunction &disj = iter->first;
            Literal head = Literal::make_atom(iter->second);
            for (size_t i = 0; i < disj.literals.size(); ++i) {
                const Literal &disjunct = disj.literals[i];
                result.push_back(Axiom::make_simple_axiom(disjunct, head));
            }
        }
        return result;
    }

};


static void error(const string &msg) {
    cerr << "error: " << msg << endl << "aborting." << endl;
    exit(1);
}


static void convert(const char *domain_filename,
                    const char *problem_filename) {
    vector<string> domain_tokens;
    tokenize(domain_filename, domain_tokens);
    /*
    for (size_t i = 0; i < domain_tokens.size(); ++i)
        cout << "*" << domain_tokens[i] << "*" << endl;
    */

    vector<string> problem_tokens;
    tokenize(problem_filename, problem_tokens);
    /*
    for (size_t i = 0; i < problem_tokens.size(); ++i)
        cout << "*" << problem_tokens[i] << "*" << endl;
    */

    Matcher domain(domain_tokens);

    // parse "(define (domain FOO)"
    cout << "parse domain header..." << endl;
    domain.match("(");
    domain.match("define");
    domain.match("(");
    domain.match("domain");
    domain.skip();
    domain.match(")");

    // parse "(:requirements ... )"
    cout << "parse requirements..." << endl;
    domain.match("(");
    domain.match(":requirements");
    while (domain.get() != ")")
        ;

    // parse "(:predicates (FOO) (BAR) ... )"
    cout << "parse predicates..." << endl;
    set<string> atoms;
    domain.match("(");
    domain.match(":predicates");
    while (domain.peek() != ")") {
        domain.match("(");
        string atom = domain.get();
        domain.match(")");
        if (atoms.count(atom)) {
            error("duplicate atom: " + atom);
        }
        atoms.insert(atom);
    }
    domain.match(")");

    cout << "Number of atoms: " << atoms.size() << endl;

    cout << "parse actions..." << endl;
    vector<Action> actions;
    while (domain.peek() != ")" && domain.peek(1) == ":action")
        actions.push_back(parse_action(domain, atoms));

    cout << "Number of actions: " << actions.size() << endl;

    cout << "parse axioms..." << endl;
    vector<Axiom> axioms;
    while (domain.peek() != ")" && domain.peek(1) == ":derived")
        axioms.push_back(parse_derived(domain, atoms));

    // parse end of domain file
    cout << "parse end of domain file..." << endl;
    domain.match(")");
    domain.match_end();


    Matcher problem(problem_tokens);

    // parse "(define (problem FOO) (:domain BAR)"
    cout << "parse problem header..." << endl;
    problem.match("(");
    problem.match("define");
    problem.match("(");
    problem.match("problem");
    problem.skip();
    problem.match(")");
    problem.match("(");
    problem.match(":domain");
    problem.skip();
    problem.match(")");

    // parse "(:init (FOO) (BAR) ... )"
    cout << "parse initial state..." << endl;
    set<string> init;
    problem.match("(");
    problem.match(":objects");
    problem.match(")");
    problem.match("(");
    problem.match(":init");
    while (problem.peek() != ")") {
        problem.match("(");
        string atom = problem.get();
        problem.match(")");
        if (!atoms.count(atom)) {
            error("unknown atom in init: " + atom);
        }
        init.insert(atom);
    }
    problem.match(")");

    // parse (:goal (and (FOO) (BAR) ...))
    problem.match("(");
    problem.match(":goal");
    Conjunction goal = parse_conjunction(problem, atoms);
    problem.match(")");

    // parse end of problem file
    cout << "parse end of problem file..." << endl;
    problem.match(")");
    problem.match_end();

    // Determine derived variables and compile away CNF axioms by
    // introducing auxiliary axioms.
    set<string> derived_vars = get_derived_variables(init, actions, axioms);
    compile_away_cnf(atoms, derived_vars, axioms);

    dump_sas_file(atoms, derived_vars, init, goal, actions, axioms);
}


static set<string> get_derived_variables(
    const set<string> &init,
    const vector<Action> &actions,
    const vector<Axiom> &axioms) {

    set<string> derived;

    // All atoms mentioned in the initial state must be fluents.
    set<string> fluents(init);

    // All atoms modified by effects must be fluents.
    for (size_t i = 0; i < actions.size(); ++i) {
        for (size_t j = 0; j < actions[i].effects.size(); ++j) {
            const Literal &eff = actions[i].effects[j].effect;
            fluents.insert(eff.atom);
        }
    }


    // All atoms appearing in the head of axioms must be derived, and
    // all atoms appearing negated in the body of axioms must not be
    // derived. (The latter is a restriction of this preprocessor: in
    // general, such negations are allowed if the rules are
    // stratifiable.)
    for (size_t i = 0; i < axioms.size(); ++i) {
        const Axiom &axiom = axioms[i];
        derived.insert(axiom.head.atom);
        const CNF &body = axiom.body;
        for (size_t j = 0; j < body.clauses.size(); ++j) {
            const Disjunction &clause = body.clauses[j];
            for (size_t k = 0; k < clause.literals.size(); ++k) {
                const Literal &lit = clause.literals[k];
                if (lit.negated) {
                    fluents.insert(lit.atom);
                }
            }
        }
    }

        cout << "Number of fluents: " << fluents.size() << endl;


    vector<string> common_elements;
    set_intersection(fluents.begin(), fluents.end(),
                     derived.begin(), derived.end(),
                     back_inserter(common_elements));
    if (!common_elements.empty())
        error("fluents overlap with derived variables: " + common_elements[0]);

    return derived;
}


static void compile_away_cnf(
    set<string> &atoms,
    set<string> &derived_vars,
    vector<Axiom> &axioms) {

    AuxiliaryAxiomGenerator aux_axiom_gen;
    for (size_t i = 0; i < axioms.size(); ++i) {
        Axiom &axiom = axioms[i];
        CNF &body = axiom.body;
        for (size_t j = 0; j < body.clauses.size(); ++j) {
            Disjunction &clause = body.clauses[j];
            if (clause.literals.size() != 1) {
                string clause_axiom = aux_axiom_gen.
                    get_auxiliary_atom_for_disjunction(clause);
                // cout << "replacing disjunction with " << clause_axiom << endl;
                clause.literals.clear();
                clause.literals.push_back(Literal::make_atom(clause_axiom));
            }
        }
    }

    vector<string> new_derived_vars = aux_axiom_gen.get_auxiliary_atoms();
    atoms.insert(new_derived_vars.begin(), new_derived_vars.end());
    derived_vars.insert(new_derived_vars.begin(), new_derived_vars.end());

    vector<Axiom> aux_axioms = aux_axiom_gen.get_auxiliary_axioms();
    axioms.insert(axioms.end(), aux_axioms.begin(), aux_axioms.end());
}


static void dump_sas_file(
    const set<string> &atoms,
    const set<string> &derived_vars,
    const set<string> &init,
    const Conjunction &goal,
    const vector<Action> &actions,
    const vector<Axiom> &axioms) {

    cout << "Writing output.sas..." << endl;
    ofstream sas_file("output.sas");

    sas_file << "begin_version" << endl << 3 << endl << "end_version" << endl;
    sas_file << "begin_metric" << endl << 0 << endl << "end_metric" << endl;

    int num_vars = 0;
    map<string, int> atom_to_varno;
    vector<string> varno_to_atom;
    for (set<string>::const_iterator iter = atoms.begin();
         iter != atoms.end(); ++iter) {
        varno_to_atom.push_back(*iter);
        atom_to_varno[*iter] = num_vars++;
    }

    sas_file << num_vars << endl;
    for (int i = 0; i != num_vars; ++i) {
        const string &atom = varno_to_atom[i];
        int axiom_layer = derived_vars.count(atom) ? 0 : -1;
        sas_file << "begin_variable" << endl
                 << "var" << i << endl
                 << axiom_layer << endl
                 << 2 << endl
                 << "Atom " << atom << "()" << endl
                 << "NegatedAtom " << atom << "()" << endl
                 << "end_variable" << endl;
    }

    sas_file << 0 << endl;

    sas_file << "begin_state" << endl;
    for (int i = 0; i != num_vars; ++i) {
        const string &atom = varno_to_atom[i];
        int initial_value = init.count(atom) ? 0 : 1;
        sas_file << initial_value << endl;
    }
    sas_file << "end_state" << endl;

    vector<pair<int, int> > goal_pairs = convert_conjunction(
        goal, atom_to_varno);
    sas_file << "begin_goal" << endl
             << goal_pairs.size() << endl;
    for (size_t i = 0; i < goal_pairs.size(); ++i)
        sas_file << goal_pairs[i].first << " " << goal_pairs[i].second << endl;
    sas_file << "end_goal" << endl;

    sas_file << actions.size() << endl;
    for (size_t i = 0; i < actions.size(); ++i) {
        const Action &action = actions[i];
        vector<pair<int, int> > pre_prevail = convert_conjunction(
            action.precondition, atom_to_varno);
        set<int> eff_vars;
        for (size_t j = 0; j < action.effects.size(); ++j) {
            int eff_var = atom_to_varno[action.effects[j].effect.atom];
            eff_vars.insert(eff_var);
        }

        vector<pair<int, int> > prevail;
        map<int, int> var_to_pre;
        for (size_t j = 0; j < pre_prevail.size(); ++j) {
            int varno = pre_prevail[j].first;
            int value = pre_prevail[j].second;
            if (eff_vars.count(varno)) {
                var_to_pre[varno] = value;
            } else {
                prevail.push_back(pre_prevail[j]);
            }
        }

        sas_file << "begin_operator" << endl
             << action.name << endl
             << prevail.size() << endl;
        for (size_t j = 0; j < prevail.size(); ++j)
            sas_file << prevail[j].first << " " << prevail[j].second << endl;
        sas_file << action.effects.size() << endl;
        for (size_t j = 0; j < action.effects.size(); ++j) {
            const Effect &eff = action.effects[j];
            vector<pair<int, int> > eff_cond = convert_conjunction(
                eff.conditions, atom_to_varno);
            int eff_var = atom_to_varno[eff.effect.atom];
            int post_val = eff.effect.negated ? 1 : 0;
            sas_file << eff_cond.size();
            for (size_t k = 0; k < eff_cond.size(); ++k)
                sas_file << " " << eff_cond[k].first
                         << " " << eff_cond[k].second;
            int pre_val = -1;
            if (var_to_pre.count(eff_var))
                pre_val = var_to_pre[eff_var];
            sas_file << " " << eff_var << " " << pre_val << " "
                     << post_val << endl;
        }
        sas_file << 0 << endl
                 << "end_operator" << endl;
    }

    sas_file << axioms.size() << endl;
    for (size_t i = 0; i < axioms.size(); ++i) {
        const Axiom &axiom = axioms[i];
        pair<int, int> head = convert_literal(axiom.head, atom_to_varno);
        assert(head.second == 0);
        vector<pair<int, int> > body;
        for (size_t j = 0; j < axiom.body.clauses.size(); ++j) {
            const Disjunction &clause = axiom.body.clauses[j];
            assert(clause.literals.size() == 1);
            body.push_back(convert_literal(clause.literals[0], atom_to_varno));
        }

        sas_file << "begin_rule" << endl
             << body.size() << endl;
        for (size_t j = 0; j < body.size(); ++j)
            sas_file << body[j].first << " " << body[j].second << endl;
        sas_file << head.first << " 1 0" << endl
             << "end_rule" << endl;
    }
}


static pair<int, int> convert_literal(const Literal &literal,
                                      const map<string, int> &atom_to_varno) {
    int varno = atom_to_varno.at(literal.atom);
    int value = literal.negated ? 1 : 0;
    return make_pair(varno, value);
}


static vector<pair<int, int> > convert_conjunction(
    const Conjunction &conj, const map<string, int> &atom_to_varno) {
    set<int> included_vars;
    vector<pair<int, int> > result;
    for (size_t i = 0; i < conj.literals.size(); ++i) {
        pair<int, int> conv = convert_literal(conj.literals[i], atom_to_varno);
        int varno = conv.first;
        if (!included_vars.count(varno)) {
	  	    //error("conjunction with duplicate variable");

        included_vars.insert(varno);
        result.push_back(conv);
	}
    }
    sort(result.begin(), result.end());
    return result;
}


static Literal parse_atom(Matcher &matcher, const set<string> &atoms) {
    matcher.match("(");
    string atom = matcher.get();
    if (!atoms.count(atom))
        error(string("unknown atom: ") + atom);
    matcher.match(")");
    return Literal::make_atom(atom);
}


static Literal parse_literal(Matcher &matcher, const set<string> &atoms) {
    if (matcher.peek() == "(" && matcher.peek(1) == "not") {
        matcher.match("(");
        matcher.match("not");
        Literal result = parse_atom(matcher, atoms);
        result.negated = true;
        matcher.match(")");
        return result;
    } else {
        return parse_atom(matcher, atoms);
    }
}


static Conjunction parse_conjunction(Matcher &matcher,
                                     const set<string> &atoms) {
    Conjunction result;
    if (matcher.peek(1) == "and") {
        matcher.match("(");
        matcher.match("and");
        while (matcher.peek() != ")")
            result.literals.push_back(parse_literal(matcher, atoms));
        matcher.match(")");
    } else {
        result.literals.push_back(parse_literal(matcher, atoms));
    }
    return result;
}


static Disjunction parse_disjunction(Matcher &matcher,
                                     const set<string> &atoms) {
    Disjunction result;
    if (matcher.peek(1) == "or") {
        matcher.match("(");
        matcher.match("or");
        while (matcher.peek() != ")")
            result.literals.push_back(parse_literal(matcher, atoms));
        matcher.match(")");
    } else {
        result.literals.push_back(parse_literal(matcher, atoms));
    }
    return result;
}


static CNF parse_cnf(Matcher &matcher, const set<string> &atoms) {
    CNF result;
    if (matcher.peek(1) == "and") {
        matcher.match("(");
        matcher.match("and");
        while (matcher.peek() != ")")
            result.clauses.push_back(parse_disjunction(matcher, atoms));
        matcher.match(")");
    } else {
        result.clauses.push_back(parse_disjunction(matcher, atoms));
    }
    return result;
}


static Action parse_action(Matcher &matcher, const set<string> &atoms) {
    // parse "(:action ... )"

    // cout << "parsing action..." << endl;

    Action action;
    matcher.match("(");
    matcher.match(":action");
    action.name = matcher.get();
    // cout << action.name << endl;

    if (matcher.peek() == ":parameters") {
        matcher.match(":parameters");
        matcher.match("(");
        matcher.match(")");
    }

    if (matcher.peek() == ":precondition") {
        matcher.match(":precondition");
        action.precondition = parse_conjunction(matcher, atoms);
    } else {
        action.precondition = Conjunction();
    }

    matcher.match(":effect");
    matcher.match("(");
    matcher.match("and");

    while (matcher.peek() != ")") {
        if (matcher.peek(1) == "when") {
            matcher.match("(");
            matcher.match("when");
            Conjunction eff_cond = parse_conjunction(matcher, atoms);
            Conjunction eff_lits = parse_conjunction(matcher, atoms);
            for (size_t i = 0; i < eff_lits.literals.size(); ++i) {
                Effect eff;
                eff.conditions = eff_cond;
                eff.effect = eff_lits.literals[i];
                action.effects.push_back(eff);
            }
            matcher.match(")");
        } else {
            Effect eff;
            eff.effect = parse_literal(matcher, atoms);
            action.effects.push_back(eff);
        }
    }

    matcher.match(")");
    matcher.match(")");

    return action;
}


static Axiom parse_derived(Matcher &matcher, const set<string> &atoms) {
    // parse "(:derived ATOM FORMULA)"
    // where ATOM is a ground atom and FORMULA is a CNF formula.
    // Negative literals in FORMULA are only allowed if they refer to
    // fluents, not to derived predicates.

    // cout << "parsing axiom..." << endl;

    Axiom axiom;
    matcher.match("(");
    matcher.match(":derived");

    if (matcher.peek() == ":parameters") {
        matcher.match(":parameters");
        matcher.match("(");
        matcher.match(")");
    }

    axiom.head = parse_atom(matcher, atoms);
    axiom.body = parse_cnf(matcher, atoms);
    matcher.match(")");

    return axiom;
}


static void tokenize(const char *filename, vector<string> &result) {
    ifstream file(filename);
    while (true) {
        if (file.eof())
            return;
        if (file.fail())
            error(string("problem reading ") + filename);
        string line;
        getline(file, line);

        // Tokenize line.
        size_t pos = 0;
        while (pos < line.length()) {
            // If we're at a separator character, advance one character.
            // Otherwise, advance to the next separator character.
            size_t next_pos = line.find_first_of("() \t\n;", pos);
            if (next_pos == pos)
                next_pos += 1;
            if (next_pos == string::npos)
                next_pos = line.length();
            string token = line.substr(pos, next_pos - pos);
            if (token[0] == ';')
                break;
            if (token[0] != ' ' && token[0] != '\t' &&
                token[0] != '\n')
                result.push_back(token);
            pos = next_pos;
        }
    }
}


int main(int argc, char **argv) {
    if (argc != 3) {
        cerr << "usage: " << argv[0] << " DOMAIN_FILE PROBLEM_FILE" << endl;
        exit(2);
    }
    try {
        convert(argv[1], argv[2]);
    } catch (const exception &e) {
        error(e.what());
    }
}
