1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 8125
parse actions...
Number of actions: 206690
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 6215
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
5000 variables of 8125 necessary
0 of 0 mutex groups necessary.
206663 of 206690 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 10000
Preprocessor derived variables: 0
Preprocessor task size: 6781292
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=24.88s]
building causal graph...done! [t=30.98s]
packing state variables...done! [t=30.98s]
Variables: 5000
Facts: 10000
Bytes per state: 628
done initalizing global data [t=30.98s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 3048045 unary operators... done! [1677551 unary operators]
Best heuristic value: 11 [g=0, 1 evaluated, 0 expanded, t=33.20s, 1299476 KB]
Best heuristic value: 10 [g=1, 2 evaluated, 1 expanded, t=33.42s, 1299476 KB]
Best heuristic value: 9 [g=2, 3 evaluated, 2 expanded, t=33.62s, 1299476 KB]
Best heuristic value: 8 [g=3, 4 evaluated, 3 expanded, t=33.82s, 1299476 KB]
Best heuristic value: 7 [g=4, 5 evaluated, 4 expanded, t=34.02s, 1299476 KB]
Best heuristic value: 6 [g=5, 6 evaluated, 5 expanded, t=34.22s, 1299476 KB]
Best heuristic value: 5 [g=6, 7 evaluated, 6 expanded, t=34.40s, 1299476 KB]
Best heuristic value: 4 [g=7, 8 evaluated, 7 expanded, t=34.60s, 1299476 KB]
Best heuristic value: 3 [g=8, 9 evaluated, 8 expanded, t=34.80s, 1299476 KB]
Best heuristic value: 2 [g=9, 10 evaluated, 9 expanded, t=34.98s, 1299476 KB]
Best heuristic value: 1 [g=10, 11 evaluated, 10 expanded, t=35.18s, 1299476 KB]
Solution found!
Actual search time: 4.22s [t=35.20s]
tr-c549-N-c546 (1)
tr-c546-N-c543 (1)
tr-c543-NW-c500 (1)
tr-c500-NW-c454 (1)
tr-c454-NW-c408 (1)
tr-c408-N-c345 (1)
tr-c345-N-c282 (1)
tr-c282-N-c219 (1)
tr-c219-N-c156 (1)
g-c156-o0-c952 (1)
dummy-grasping-o0-action (1)
Plan length: 11 step(s).
Plan cost: 11
Initial state h value: 11.
Expanded 11 state(s).
Reopened 0 state(s).
Evaluated 12 state(s).
Evaluations: 12
Generated 48 state(s).
Dead ends: 0 state(s).
Search time: 4.22s
Total time: 35.20s
Solution found.
Peak memory: 1299476 KB

Plans and cost: 

