1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2083
parse actions...
Number of actions: 10488
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1201
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1358 variables of 2083 necessary
0 of 0 mutex groups necessary.
10227 of 10488 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 2716
Preprocessor derived variables: 0
Preprocessor task size: 112086
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.36s]
building causal graph...done! [t=0.40s]
packing state variables...done! [t=0.40s]
Variables: 1358
Facts: 2716
Bytes per state: 172
done initalizing global data [t=0.40s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 50399 unary operators... done! [36694 unary operators]
Best heuristic value: 8 [g=0, 1 evaluated, 0 expanded, t=0.48s, 23516 KB]
Best heuristic value: 7 [g=1, 2 evaluated, 1 expanded, t=0.48s, 23516 KB]
Best heuristic value: 6 [g=2, 3 evaluated, 2 expanded, t=0.48s, 23516 KB]
Best heuristic value: 5 [g=4, 7 evaluated, 6 expanded, t=0.48s, 23516 KB]
Best heuristic value: 4 [g=5, 8 evaluated, 7 expanded, t=0.48s, 23516 KB]
Best heuristic value: 3 [g=6, 9 evaluated, 8 expanded, t=0.48s, 23516 KB]
Best heuristic value: 2 [g=7, 10 evaluated, 9 expanded, t=0.48s, 23516 KB]
Best heuristic value: 1 [g=8, 11 evaluated, 10 expanded, t=0.48s, 23516 KB]
Solution found!
Actual search time: 0.08s [t=0.48s]
tr-c35-W-c32 (1)
rr-c32-CCW-c98 (1)
g-c98-o5-c17 (1)
ro-c98-c30-CCW-o5-c17-c10 (1)
p-o5 (1)
tr-c30-E-c33 (1)
g-c33-o3-c11 (1)
to-c33-c17-N-o3-c11-c155 (1)
dummy-action-17 (1)
Plan length: 9 step(s).
Plan cost: 9
Initial state h value: 8.
Expanded 11 state(s).
Reopened 0 state(s).
Evaluated 12 state(s).
Evaluations: 12
Generated 47 state(s).
Dead ends: 0 state(s).
Search time: 0.08s
Total time: 0.48s
Solution found.
Peak memory: 23516 KB

Plans and cost: 

