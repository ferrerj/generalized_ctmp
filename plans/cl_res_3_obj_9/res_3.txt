1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 9319
parse actions...
Number of actions: 97189
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5829
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
5456 variables of 9319 necessary
0 of 0 mutex groups necessary.
96423 of 97189 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 10912
Preprocessor derived variables: 0
Preprocessor task size: 2420148
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=8.76s]
building causal graph...done! [t=10.66s]
packing state variables...done! [t=10.66s]
Variables: 5456
Facts: 10912
Bytes per state: 684
done initalizing global data [t=10.66s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 1220315 unary operators... done! [780041 unary operators]
Best heuristic value: 35 [g=0, 1 evaluated, 0 expanded, t=11.48s, 500892 KB]
Best heuristic value: 32 [g=4, 6 evaluated, 5 expanded, t=11.70s, 500892 KB]
Best heuristic value: 29 [g=9, 16 evaluated, 15 expanded, t=12.14s, 500892 KB]
Best heuristic value: 28 [g=17, 33 evaluated, 32 expanded, t=12.90s, 500892 KB]
Best heuristic value: 26 [g=18, 34 evaluated, 33 expanded, t=12.94s, 500892 KB]
Best heuristic value: 25 [g=26, 56 evaluated, 55 expanded, t=13.98s, 500892 KB]
Best heuristic value: 24 [g=27, 57 evaluated, 56 expanded, t=14.02s, 500892 KB]
Switch from bucket-based to heap-based queue at key = 7393, num_pushes = 6501
Best heuristic value: 23 [g=33, 71 evaluated, 70 expanded, t=14.66s, 500892 KB]
Best heuristic value: 22 [g=34, 73 evaluated, 72 expanded, t=14.76s, 500892 KB]
Best heuristic value: 20 [g=39, 90 evaluated, 89 expanded, t=15.52s, 500892 KB]
Best heuristic value: 18 [g=40, 91 evaluated, 90 expanded, t=15.56s, 500892 KB]
Best heuristic value: 16 [g=54, 135 evaluated, 134 expanded, t=17.50s, 500892 KB]
Best heuristic value: 15 [g=56, 139 evaluated, 138 expanded, t=17.68s, 500892 KB]
Best heuristic value: 11 [g=61, 150 evaluated, 149 expanded, t=18.14s, 500892 KB]
Best heuristic value: 8 [g=62, 152 evaluated, 151 expanded, t=18.22s, 500892 KB]
Best heuristic value: 7 [g=63, 153 evaluated, 152 expanded, t=18.26s, 500892 KB]
Best heuristic value: 6 [g=64, 154 evaluated, 153 expanded, t=18.30s, 500892 KB]
Best heuristic value: 5 [g=65, 155 evaluated, 154 expanded, t=18.34s, 500892 KB]
Best heuristic value: 4 [g=67, 157 evaluated, 156 expanded, t=18.42s, 500892 KB]
Best heuristic value: 3 [g=68, 158 evaluated, 157 expanded, t=18.46s, 500892 KB]
Best heuristic value: 2 [g=69, 159 evaluated, 158 expanded, t=18.48s, 500892 KB]
Best heuristic value: 1 [g=70, 160 evaluated, 159 expanded, t=18.52s, 500892 KB]
Solution found!
Actual search time: 7.86s [t=18.52s]
tr-c176-S-c185 (1)
tr-c185-S-c194 (1)
rr-c194-CCW-c364 (1)
rr-c364-CCW-c192 (1)
tr-c192-SW-c200 (1)
tr-c200-W-c199 (1)
tr-c199-NW-c189 (1)
g-c189-o6-c129 (1)
to-c189-c198-S-o6-c129-c144 (1)
p-o6 (1)
tr-c198-SE-c208 (1)
tr-c208-NE-c200 (1)
tr-c200-NE-c192 (1)
tr-c192-SE-c204 (1)
tr-c204-E-c205 (1)
tr-c205-NE-c197 (1)
g-c197-o7-c141 (1)
to-c197-c206-S-o7-c141-c152 (1)
to-c206-c215-S-o7-c152-c161 (1)
to-c215-c224-S-o7-c161-c170 (1)
to-c224-c223-W-o7-c170-c169 (1)
to-c223-c222-W-o7-c169-c168 (1)
to-c222-c219-W-o7-c168-c165 (1)
ro-c219-c378-CCW-o7-c165-c189 (1)
to-c378-c374-N-o7-c189-c180 (1)
ro-c374-c210-CW-o7-c180-c165 (1)
ro-c210-c372-CW-o7-c165-c188 (1)
p-o7 (1)
tr-c372-N-c368 (1)
rr-c368-CCW-c201 (1)
tr-c201-W-c200 (1)
tr-c200-W-c199 (1)
tr-c199-W-c198 (1)
g-c198-o6-c144 (1)
to-c198-c207-S-o6-c144-c153 (1)
to-c207-c216-S-o6-c153-c162 (1)
to-c216-c208-NE-o6-c162-c163 (1)
to-c208-c218-SE-o6-c163-c164 (1)
to-c218-c210-NE-o6-c164-c165 (1)
ro-c210-c374-CCW-o6-c165-c180 (1)
p-o6 (1)
rr-c374-CW-c210 (1)
rr-c210-CW-c372 (1)
g-c372-o7-c188 (1)
ro-c372-c210-CCW-o7-c188-c165 (1)
p-o7 (1)
rr-c210-CCW-c374 (1)
g-c374-o6-c180 (1)
to-c374-c378-S-o6-c180-c189 (1)
p-o6 (1)
rr-c378-CW-c219 (1)
g-c219-o7-c165 (1)
ro-c219-c376-CW-o7-c165-c197 (1)
to-c376-c372-N-o7-c197-c188 (1)
to-c372-c368-N-o7-c188-c179 (1)
p-o7 (1)
tr-c368-N-c364 (1)
rr-c364-CCW-c192 (1)
g-c192-o5-c132 (1)
ro-c192-c366-CCW-o5-c132-c162 (1)
to-c366-c362-N-o5-c162-c153 (1)
to-c362-c354-N-o5-c153-c144 (1)
to-c354-c346-N-o5-c144-c129 (1)
p-o5 (1)
rr-c346-CW-c165 (1)
g-c165-o1-c87 (1)
ro-c165-c344-CW-o1-c87-c141 (1)
p-o1 (1)
tr-c344-N-c336 (1)
tr-c336-N-c326 (1)
dummy-action-326 (1)
Plan length: 71 step(s).
Plan cost: 71
Initial state h value: 35.
Expanded 160 state(s).
Reopened 0 state(s).
Evaluated 161 state(s).
Evaluations: 161
Generated 769 state(s).
Dead ends: 0 state(s).
Search time: 7.86s
Total time: 18.52s
Solution found.
Peak memory: 500892 KB

Plans and cost: 

