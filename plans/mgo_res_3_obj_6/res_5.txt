1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 8551
parse actions...
Number of actions: 66310
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5341
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4662 variables of 8551 necessary
0 of 0 mutex groups necessary.
65280 of 66310 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 9324
Preprocessor derived variables: 0
Preprocessor task size: 1489433
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=5.90s]
building causal graph...done! [t=7.12s]
packing state variables...done! [t=7.12s]
Variables: 4662
Facts: 9324
Bytes per state: 584
done initalizing global data [t=7.12s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 691278 unary operators... done! [527844 unary operators]
Best heuristic value: 23 [g=0, 1 evaluated, 0 expanded, t=7.66s, 340152 KB]
Best heuristic value: 22 [g=1, 2 evaluated, 1 expanded, t=7.68s, 340152 KB]
Best heuristic value: 21 [g=2, 3 evaluated, 2 expanded, t=7.72s, 340152 KB]
Best heuristic value: 20 [g=3, 4 evaluated, 3 expanded, t=7.74s, 340152 KB]
Best heuristic value: 19 [g=4, 5 evaluated, 4 expanded, t=7.78s, 340152 KB]
Best heuristic value: 18 [g=5, 6 evaluated, 5 expanded, t=7.80s, 340152 KB]
Best heuristic value: 17 [g=6, 7 evaluated, 6 expanded, t=7.82s, 340152 KB]
Best heuristic value: 16 [g=7, 8 evaluated, 7 expanded, t=7.86s, 340152 KB]
Best heuristic value: 15 [g=8, 9 evaluated, 8 expanded, t=7.90s, 340152 KB]
Best heuristic value: 14 [g=9, 10 evaluated, 9 expanded, t=7.92s, 340152 KB]
Best heuristic value: 13 [g=10, 11 evaluated, 10 expanded, t=7.96s, 340152 KB]
Best heuristic value: 12 [g=11, 12 evaluated, 11 expanded, t=7.98s, 340152 KB]
Best heuristic value: 11 [g=12, 13 evaluated, 12 expanded, t=8.00s, 340152 KB]
Best heuristic value: 10 [g=13, 14 evaluated, 13 expanded, t=8.04s, 340152 KB]
Best heuristic value: 9 [g=14, 15 evaluated, 14 expanded, t=8.06s, 340152 KB]
Best heuristic value: 8 [g=15, 16 evaluated, 15 expanded, t=8.10s, 340152 KB]
Best heuristic value: 7 [g=16, 17 evaluated, 16 expanded, t=8.12s, 340152 KB]
Best heuristic value: 6 [g=17, 18 evaluated, 17 expanded, t=8.16s, 340152 KB]
Best heuristic value: 5 [g=18, 19 evaluated, 18 expanded, t=8.18s, 340152 KB]
Best heuristic value: 4 [g=19, 20 evaluated, 19 expanded, t=8.20s, 340152 KB]
Best heuristic value: 3 [g=20, 21 evaluated, 20 expanded, t=8.24s, 340152 KB]
Best heuristic value: 2 [g=21, 22 evaluated, 21 expanded, t=8.26s, 340152 KB]
Best heuristic value: 1 [g=22, 23 evaluated, 22 expanded, t=8.28s, 340152 KB]
Solution found!
Actual search time: 1.16s [t=8.28s]
tr-c203-W-c201 (1)
tr-c201-W-c199 (1)
tr-c199-W-c196 (1)
tr-c196-W-c193 (1)
tr-c193-SW-c239 (1)
tr-c239-S-c252 (1)
tr-c252-S-c265 (1)
rr-c265-CW-c394 (1)
rr-c394-CW-c264 (1)
rr-c264-CW-c392 (1)
rr-c392-CW-c266 (1)
tr-c266-N-c253 (1)
tr-c253-N-c240 (1)
tr-c240-NE-c194 (1)
tr-c194-E-c197 (1)
tr-c197-E-c200 (1)
tr-c200-E-c202 (1)
tr-c202-E-c204 (1)
tr-c204-E-c206 (1)
g-c206-o2-c774 (1)
to-c206-c208-E-o2-c774-c775 (1)
to-c208-c210-E-o2-c775-c776 (1)
dummy-action-210 (1)
Plan length: 23 step(s).
Plan cost: 23
Initial state h value: 23.
Expanded 23 state(s).
Reopened 0 state(s).
Evaluated 24 state(s).
Evaluations: 24
Generated 68 state(s).
Dead ends: 0 state(s).
Search time: 1.16s
Total time: 8.28s
Solution found.
Peak memory: 340152 KB

Plans and cost: 

