#include<iostream>
#include<fstream>
#include<map>
#include<vector>
#include"bin/include.h"

using namespace std;

#define MAX_SAS 7

string sas_plan[ MAX_SAS ] = { "sas_plan.1", "sas_plan.2", "sas_plan.3", "sas_plan.4" , "sas_plan.5" , "sas_plan.6" , "sas_plan.7" };
string exec_prog;
string orig_domain;
string dest_domain;
string dest_ins;
vector< int > bound;
vector< int > timeout;
int procedures;
int stack_pred;
int stack_vars;
int stack_size;
int ntest_inst;
int test_time_bound;
vector< string > vstack_pred;
vector< string > vstack_vars;
vector< vector< string > > instances;
vector< string > test_instances;
map< string , vector< string > > act2ins;

vector< string > new_instructions;
vector< vector < string > > instruction_params;

ofstream execution("execution.txt");

void init( int argc, char* argv[] ){
	if( argc < 2 ) exit( -1 );

	//Config File
	ifstream ifs( argv[ 1 ] );

	//string comment_line;

	//getline( ifs , comment_line );

	ifs >> exec_prog;

	ifs >> orig_domain;

	ifs >> dest_domain >> dest_ins;

	ifs >> procedures;

	bound.resize( procedures + 1 );

	timeout.resize( procedures + 1 );

	for( int p = 0 ; p <= procedures ; p++ ){
		int cant;

		ifs >> bound[ p ] >> timeout[ p ] >> cant;

		instances.push_back( vector< string >( cant ) );

		for( int q = 0; q < cant; q++ ){
			ifs >> instances[ p ][ q ];
		}
	}

	ifs >> ntest_inst;
	
	if( ntest_inst > 0 ){
		ifs >> test_time_bound;
	
		test_instances.resize( ntest_inst );
	
		for( int t = 0; t < ntest_inst; t++ ){
			ifs >> test_instances[ t ];
		}
	}

	ifs >> stack_pred;

	vstack_pred.resize( stack_pred );

	for( int i = 0; i < stack_pred ; i++ ){
		ifs >> vstack_pred[ i ];
	}

	ifs >> stack_vars;
	
	vstack_vars.resize( stack_vars );
	
	for( int i = 0; i < stack_vars; i++){
		ifs >> vstack_vars[ i ];
	}

	ifs >> stack_size;

	ifs.close();
}



void read_act2ins(){

	act2ins.clear();

	ifstream ifs( "act2ins.txt" );

	string line;

	while( getline( ifs, line ) ){

		string action, instr;

		vector< string > instructions;

		istringstream ss( line );

		ss >> action;

		while( ss >> instr ) instructions.push_back( instr );

		act2ins[ action ] = instructions;

	}

}

void toUpper( string &s ){
	for( int i = 0; i < int( s.length() ); i++ ){
		if(s[ i ] >= 'a' && s[ i ] <= 'z' )
			s[ i ] = s[ i ] - 'a' + 'A';	
	}
}

string get_sas_plan(){
	for( int i = MAX_SAS-1; i >= 0 ; i-- ){
		ifstream ifs( sas_plan[i].c_str() );
		if( ifs ){
			ifs.close();
			return sas_plan[ i ];
		}	
	}
	return "";
}

void call_compiler( int p , bool to_program = 1 ){ 
	ostringstream compiler;

	compiler << exec_prog << " " << orig_domain << " " << instances[ p ].size();

	for( size_t i = 0; i < instances[ p ].size(); i++ ) 
		compiler << " " << instances[ p ][ i ];

	compiler << " " << bound[ p ] << " " << p << " " << stack_pred ;
	
	for( int i = 0; i < stack_pred; i++ ){
		compiler << " " << vstack_pred[ i ] ;
	}
	
	compiler << " " << stack_vars;
	
	for( int i = 0; i < stack_vars; i++ ){
		compiler << " " << vstack_vars[ i ];	
	}
	
	compiler << " " << stack_size << " " << to_program << " > " << dest_domain << " 2> " << dest_ins;

	execution << "SYSTEM CALL: " << compiler.str() << endl;

	system( compiler.str().c_str() );
}


void call_test_compiler( bool to_program = 0 ){
	ostringstream compiler;

	compiler << exec_prog << " " << orig_domain << " " << ntest_inst;

	for( int t = 0; t < ntest_inst; t++ ){
		compiler << " " << test_instances[ t ];
	}	

	compiler << " " << stack_vars;

	for( int i = 0; i < stack_vars; i++ ){
		compiler << " " << vstack_vars[ i ];	
	}
	
	compiler << " " << stack_size << " " << to_program << " > " << dest_domain << " 2> " << dest_ins;

	execution << "TEST SYSTEM CALL: " << compiler.str() << endl;

	system( compiler.str().c_str() );
}


void call_planner( int p , int to ){
	if( p > 0 ){
		execution << "MODIFYING DOMAIN FOR THE MAIN" << endl;
		Domain *cd = new Domain( dest_domain.c_str() );
		execution << "\tDOMAIN READ" << endl;
		Instance* ins = new Instance( *cd , dest_ins.c_str() );
		execution << "\tINSTANCE READ" <<endl;
		execution << "\tREADING INSTRUCTIONS TO ADD " << new_instructions.size() << endl;
		for( int ni = 0; ni < int( new_instructions.size() ); ni++ ){ 
			execution << "\t" << ni << endl;
			execution << "\t\t" << new_instructions[ ni ] << endl;
			execution << "\t\t" << instruction_params[ ni ].size() << endl;
			ins->addInit( new_instructions[ ni ] , instruction_params[ ni ] );
			execution << "\tINSERTED" << endl;
		}
		execution << "\t INSTACES ADDED" << endl;
		ofstream ofs( dest_ins.c_str() );
		ins->PDDLPrint( ofs );
	}
	ostringstream planner;

	planner << "./planner.sh " << dest_domain << " " << dest_ins << " " << to;

	system( planner.str().c_str() );
}

string convert( string &n , int p ){
	string numbers = "0123456789";
	size_t it = string::npos, itres = string::npos;

	for(int i=0; i < 10 ; i++){
		string tofind = "-" + numbers.substr( i , 1 );
		it = n.find( tofind );
		itres = min( it, itres );
	}

	string res = n;

	if( n.substr( 0 , 4 ) != "CALL" && n.substr( 0 , 3 ) != "JMP" && n.substr( 0 , 4) != "COND" )
		res = n.substr( 0 , itres ) + "-" + string( 1 , '0' + p ) + n.substr( itres );
	else if( n.substr( 0, 3 ) == "JMP" || n.substr( 0 , 4 ) == "COND" ){
		res = n.substr( 0 , itres ) + "-" + string( 1 , '0' + p ) + n.substr( itres + 2 );
	}

	execution << "CONVERTING " << n << " TO " << res << endl;

	return res;
}

void print_instructions(){
	ofstream ofs("program.txt");
	for( int i = 0; i < int( new_instructions.size() ); i++ ){
		ofs << new_instructions[ i ] ;
		for( int j = 0; j < int( instruction_params[i].size() ); j++ ) {
			ofs << " " << instruction_params[i][j];
		}
		ofs << endl;	
	}
	ofs.close();
}

int main( int argc , char *argv[] ){

	init( argc, argv );

	int pos = 0;

	for(int p = 0; p <= procedures; p++ ){

		execution << "STARTING PROCEDURE " << p << endl;

		call_compiler( p );

		execution << "COMPILED PROCEDURE " << p << endl;

		//if( p == 0 ) 
		read_act2ins();
	
		execution << "ACTIONS FOUND: " << act2ins.size() << endl;

		call_planner( p , timeout[ p ] );

		execution << "PLANNER FINISHED IN PROCEDURE " << p << endl;

		string sp = get_sas_plan();

		execution << "EXTRACTING PLAN " << sp << endl;

		if(sp.length() == 0) return -1;
		//if( p == procedures ) break;

		ifstream itest( sp.c_str() );
		string line;

		while( getline( itest , line ) ) {
			line = line.substr( 0 , line.length() - 1 );
			size_t whitespace = line.find_first_of(" ");
			string prog = line.substr( 1 , whitespace - 1 );
			toUpper( prog );
			map< string , vector< string > >::iterator it = act2ins.find( prog );

			execution << "SEARCHING " << prog << "..." << endl;

			if( it != act2ins.end() ) {

				// vector of params
				vector< string > vp; 
				string params = line.substr( whitespace + 1 ), param;
				istringstream issp( params );
				while( issp>>param ){
					toUpper( param );
					vp.push_back( param );
				}
				// vector of instructions
				vector< string > vi = it->second; 
				for(int i = 0; i < int( vi.size() ); i++ ){
					string converted_instruction = vi[i];
					//if( p < procedures ) 
					converted_instruction = convert( vi[i] , p );	

					execution << "FOUND!!! => " << vi[i] << endl;

					new_instructions.push_back( converted_instruction );
					instruction_params.push_back( vector< string > () );

					if( i + 1 == int( vi.size() ) ){
						instruction_params[ pos ] = vp;
					}

					pos++;
				}
			}
		}
		execution << "PROCEDURE " << p << " FINISHED. " << endl;
		itest.close();
		
	}

	execution << "CALLING PRINT INSTRUCTIONS..." << endl;
	
	print_instructions();

	execution << "PRINT INSTRUCTIONS FINISHED" << endl;
	
	// TEST
	execution << "STARTING TESTS..." << endl;

	call_test_compiler( );

	//read_act2ins( );
	
	//call_planner( procedures , test_time_bound );

	execution.close();

	return 0;

}
