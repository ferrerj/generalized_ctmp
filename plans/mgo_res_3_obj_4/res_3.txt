1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6447
parse actions...
Number of actions: 45302
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 4095
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
3407 variables of 6447 necessary
0 of 0 mutex groups necessary.
44290 of 45302 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 6814
Preprocessor derived variables: 0
Preprocessor task size: 1003698
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=4.00s]
building causal graph...done! [t=4.82s]
packing state variables...done! [t=4.82s]
Variables: 3407
Facts: 6814
Bytes per state: 428
done initalizing global data [t=4.82s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 462368 unary operators... done! [353366 unary operators]
Best heuristic value: 9 [g=0, 1 evaluated, 0 expanded, t=5.18s, 212552 KB]
Best heuristic value: 8 [g=1, 2 evaluated, 1 expanded, t=5.20s, 212552 KB]
Best heuristic value: 7 [g=2, 3 evaluated, 2 expanded, t=5.22s, 212552 KB]
Best heuristic value: 6 [g=3, 4 evaluated, 3 expanded, t=5.24s, 212552 KB]
Best heuristic value: 5 [g=5, 6 evaluated, 5 expanded, t=5.26s, 212552 KB]
Best heuristic value: 3 [g=6, 7 evaluated, 6 expanded, t=5.28s, 212552 KB]
Best heuristic value: 2 [g=7, 8 evaluated, 7 expanded, t=5.30s, 212552 KB]
Best heuristic value: 1 [g=8, 9 evaluated, 8 expanded, t=5.32s, 212552 KB]
Solution found!
Actual search time: 0.50s [t=5.32s]
tr-c237-SE-c251 (1)
tr-c251-S-c264 (1)
rr-c264-CW-c392 (1)
g-c392-o0-c198 (1)
to-c392-c396-E-o0-c198-c766 (1)
to-c396-c398-E-o0-c766-c767 (1)
to-c398-c390-N-o0-c767-c738 (1)
to-c390-c374-N-o0-c738-c704 (1)
dummy-action-374 (1)
Plan length: 9 step(s).
Plan cost: 9
Initial state h value: 9.
Expanded 9 state(s).
Reopened 0 state(s).
Evaluated 10 state(s).
Evaluations: 10
Generated 78 state(s).
Dead ends: 0 state(s).
Search time: 0.50s
Total time: 5.32s
Solution found.
Peak memory: 212552 KB

Plans and cost: 

