1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 8551
parse actions...
Number of actions: 66310
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5341
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4667 variables of 8551 necessary
0 of 0 mutex groups necessary.
65296 of 66310 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 9334
Preprocessor derived variables: 0
Preprocessor task size: 1489496
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=5.88s]
building causal graph...done! [t=7.12s]
packing state variables...done! [t=7.12s]
Variables: 4667
Facts: 9334
Bytes per state: 584
done initalizing global data [t=7.12s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 691294 unary operators... done! [527860 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=7.68s, 340172 KB]
Best heuristic value: 5 [g=63, 303 evaluated, 302 expanded, t=16.30s, 340172 KB]
Best heuristic value: 4 [g=64, 304 evaluated, 303 expanded, t=16.32s, 340172 KB]
Best heuristic value: 3 [g=65, 305 evaluated, 304 expanded, t=16.34s, 340172 KB]
Best heuristic value: 2 [g=66, 306 evaluated, 305 expanded, t=16.36s, 340172 KB]
Best heuristic value: 1 [g=67, 307 evaluated, 306 expanded, t=16.38s, 340172 KB]
Solution found!
Actual search time: 9.30s [t=16.42s]
to-c222-c220-W-o2-c233-c232 (1)
to-c220-c218-W-o2-c232-c229 (1)
to-c218-c216-W-o2-c229-c779 (1)
to-c216-c214-W-o2-c779-c778 (1)
to-c214-c212-W-o2-c778-c777 (1)
to-c212-c210-W-o2-c777-c776 (1)
to-c210-c208-W-o2-c776-c775 (1)
to-c208-c206-W-o2-c775-c774 (1)
to-c206-c204-W-o2-c774-c773 (1)
to-c204-c202-W-o2-c773-c772 (1)
to-c202-c200-W-o2-c772-c771 (1)
to-c200-c197-W-o2-c771-c770 (1)
to-c197-c194-W-o2-c770-c769 (1)
to-c194-c191-W-o2-c769-c768 (1)
p-o2 (1)
tr-c191-S-c240 (1)
tr-c240-S-c253 (1)
tr-c253-S-c266 (1)
rr-c266-CCW-c392 (1)
tr-c392-SE-c404 (1)
tr-c404-E-c406 (1)
g-c406-o2-c768 (1)
to-c406-c398-N-o2-c768-c767 (1)
to-c398-c396-W-o2-c767-c766 (1)
to-c396-c392-W-o2-c766-c198 (1)
to-c392-c400-S-o2-c198-c243 (1)
to-c400-c408-S-o2-c243-c256 (1)
ro-c408-c290-CCW-o2-c256-c189 (1)
ro-c290-c410-CCW-o2-c189-c248 (1)
ro-c410-c290-CW-o2-c248-c191 (1)
p-o2 (1)
rr-c290-CW-c408 (1)
rr-c408-CW-c292 (1)
tr-c292-S-c305 (1)
rr-c305-CCW-c412 (1)
rr-c412-CCW-c303 (1)
g-c303-o2-c191 (1)
ro-c303-c414-CCW-o2-c191-c261 (1)
p-o2 (1)
tr-c414-N-c410 (1)
tr-c410-N-c402 (1)
tr-c402-N-c394 (1)
rr-c394-CW-c264 (1)
g-c264-o0-c669 (1)
ro-c264-c394-CCW-o0-c669-c186 (1)
to-c394-c386-N-o0-c186-c145 (1)
to-c386-c370-N-o0-c145-c104 (1)
to-c370-c354-N-o0-c104-c60 (1)
p-o0 (1)
rr-c354-CCW-c190 (1)
tr-c190-E-c193 (1)
tr-c193-E-c196 (1)
tr-c196-E-c199 (1)
tr-c199-E-c201 (1)
tr-c201-E-c203 (1)
tr-c203-E-c205 (1)
tr-c205-E-c207 (1)
tr-c207-E-c209 (1)
tr-c209-E-c211 (1)
tr-c211-E-c213 (1)
tr-c213-E-c215 (1)
tr-c215-E-c217 (1)
tr-c217-E-c219 (1)
tr-c219-E-c221 (1)
tr-c221-E-c223 (1)
tr-c223-E-c225 (1)
tr-c225-E-c227 (1)
dummy-action-227 (1)
Plan length: 68 step(s).
Plan cost: 68
Initial state h value: 6.
Expanded 307 state(s).
Reopened 0 state(s).
Evaluated 308 state(s).
Evaluations: 308
Generated 952 state(s).
Dead ends: 0 state(s).
Search time: 9.30s
Total time: 16.42s
Solution found.
Peak memory: 340172 KB

Plans and cost: 

