#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <fstream>

using namespace std;


map<int, pair<string, vector<int> > > plan;

set<pair<int, int>> positions;
map<pair<int, int>, string> dummies;

struct set_cmpr
{
    bool operator ()(const pair<int, int> &a, const pair<int, int> &b) {
        return a.second < b.second;
    }
};

void init() {

	string dummy, pos;
	ifstream ifs( "dummies.data" );
	while(ifs >> dummy >> pos) {

		size_t from = pos.find_first_of( "(" ) + 1;
		size_t last = pos.find_last_of( ")" ) - 1;
		pos = pos.substr(from, last);
		size_t delimiter = pos.find_first_of(",");
		int x = atoi( (pos.substr(0,delimiter)).c_str());
		int y = atoi( (pos.substr(delimiter+1)).c_str());
		positions.insert(make_pair(x,y));
		dummies.insert(make_pair(make_pair(x,y), dummy));
	}

	ifs.close();
}


//Translate an action to a dummy goal for the task planner
string translate(const string& action, vector<int>& params) {

	int x, y = 0;
	if(action.compare("xinc") == 0) {
		x = params.at(1);
		y = params.at(2);
	}
	else if(action.compare("xdec") == 0) {
		x = params.at(0);
		y = params.at(2);
	}
	else if(action.compare("yinc") == 0) {
		x = params.at(2);
		y = params.at(1);
	}
	else if(action.compare("ydec") == 0) {
		x = params.at(2);
		y = params.at(0);
	}
	return "(" + dummies.find(make_pair(x,y))->second + ")";
}



void print() {

	map<int, pair<string, vector<int> > >::iterator it;
	for( it = plan.begin(); it != plan.end(); ++it ) {
		cout << translate(it->second.first, it->second.second) << " " << 0 << " " << it->second.first << endl;
		//cout << it->second.first << ": " << it->second.second.at(0) << "," << it->second.second.at(1) << "," << it->second.second.at(2) << std::endl;
	}
}




int main() {


	string line, word;
	int k = 0;
	while( getline( cin , line ) ) {
		stringstream iss( line );
		string act;
		vector< int > params;
		iss >> act;
		size_t from = act.find_first_of( "-" ) + 1;
		size_t last = act.find_last_of( "-" ) - 1;
		last = act.find_last_of( "-" , last ) ;
		size_t tam = last - from ;
		act = act.substr( from, tam );
		if( act == "xinc" || act == "xdec" || act == "yinc" || act == "ydec"){
			while( iss >> word ){
				if(word != "row0)") {
					params.push_back( stoi(word.c_str()) );
				}
			}	
			plan.insert( make_pair( k, make_pair( act , params ) ) );
			k++;
		}
				params.clear();
	
	}
	
	init();
	print();
}
