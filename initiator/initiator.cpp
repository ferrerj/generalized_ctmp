#include "include.h"
#include<iostream>
#include<string>
#include<sstream>
#include<cstdlib>
#include<fstream>
#include<set>
#include<locale>

using namespace std;

string domain;
string instance;
string outfile = "adddel.txt";

int main(int argc, char * argv[]){
	//--------------------------- READING INPUT --------------------------------
	domain = string( argv[ 1 ] );
	instance = string( argv[ 2 ] );
	string fplan = string( argv[ 3 ] );
	int k_goal = atoi( argv[ 4 ] );
	
	//--------------------------- INIT VARIABLES --------------------------------
	Domain *d = new Domain( domain.c_str() ); 
	Instance *ins = new Instance( *d, instance.c_str() );
	Instance *ins_goal = new Instance( *d );
	ins_goal->name = "INSGOAL";


	
	//--------------------------- LAST STATE INIT EXTRACTION --------------------------------
	//ifstream ifs( outfile.c_str() );
	set< string > final_state;
	set< string >::iterator it;
	locale loc;

	for( size_t f = 0; f < ins->init.size(); f++ ){
		final_state.insert( ins->init[ f ]->name );
	}

	ifstream ifs( fplan.c_str() );
	string act;
	while( ifs >> act ){
		if(  act[0] == ';' )
			break;

		std::transform(act.begin(), act.end(),act.begin(), ::toupper);
		std::string delimiter = ")";
		act = act.substr(0, act.find(delimiter));

		Action *a = d->actions.get( act.substr( 1 ) );

		GroundVec deletes = a->deleteEffects();
		GroundVec adds = a->addEffects();
		
		for( size_t f = 0; f < deletes.size(); f++ ){
			it = final_state.find( deletes[ f ]->name );
			if( it != final_state.end() ){
				final_state.erase( it );
			}			
		}
		for( size_t f = 0; f < adds.size(); f++ ){
			final_state.insert( adds[ f ]->name );
		}
	}
	
	/*string fluent;
	ifs >> fluent;
	while( ifs >> fluent , fluent != ")"){
		final_state.insert( fluent );
	}*/
	
	for( it = final_state.begin(); it != final_state.end(); it++ ){
		string name = *it;
		for( size_t c = 0; c < name.size(); c++ )
			name[ c ] = toupper( name[ c ], loc );
		//ins_goal->addInit( name.substr( 1 , name.size() - 2 ) );
		ins_goal->addInit( name );
	}
	
	//--------------------------- GOAL TO ADD --------------------------------
	ifstream goalIFS( "plan.out" );
	vector< string > goal( k_goal );
	vector< string > keyword( k_goal );
	vector< int > isConsistent( k_goal );
	size_t k = 0;
	int is_const;
	while( goalIFS >> goal[ k ] >> is_const >> keyword[ k ] ){
		k_goal--; 
		if( k_goal == 0 ) 
			break;
		isConsistent[ k ] = is_const;
		k++;
	}

	// Transform keyword to upper
	for( size_t c = 0; c < keyword[ k ].size(); c++ ){
		keyword[ k ][ c ] = toupper( keyword[ k ][ c ], loc );
	}
	for( size_t f = 0; f < ins->goal.size(); f++ ){
		string name = "(" + ins->goal[ f ]->name + ")";

		for( size_t c = 0; c < name.size(); c++ )
				name[ c ] = tolower( name[ c ], loc );
		int consis = 0;
		for( size_t g = 0; g < goal.size(); g++ ){
			if( goal[ g ] == name ){
				consis = isConsistent[ g ]; 
				break;
			}
		}
		if( consis ){
			ins_goal->addGoal( ins->goal[ f ]->name );
		}
	}
	// If it is not consistent, delete previous goals of the same kind
	/*if( !consistent ){
		for( size_t f = 0; f < ins->goal.size(); f++ ){
			if( ins->goal[ f ]->name.find( keyword ) == string::npos ){
				ins_goal->addGoal( ins->goal[ f ]->name );
			}
		}
	}
	else{
		ins_goal->goal = ins->goal;
	}*/
	// Transform goal to upper
	string goal_name = goal[ k ].substr( 1 , goal[ k ].size() - 2 );
	for( size_t c = 0; c < goal_name.size(); c++ ){
		goal_name[ c ] = toupper( goal_name[ c ] , loc );
	}
	// Add new goal
	ins_goal->addGoal( goal_name );

	//--------------------------- PRINT NEW INSTANCE --------------------------------
	ofstream ins2ofs( "ins2.pddl" );
	ins_goal->PDDLPrint( ins2ofs );
	ins2ofs.close();
	ifstream ins2ifs( "ins2.pddl" );
	string line;
	vector< string > output;
	while( getline( ins2ifs, line ) ){
		for( size_t c = 0; c < line.size(); c++ ){
			line[ c ] = tolower( line[ c ], loc );
		}
		output.push_back( line );
	}
	ins2ifs.close();
	ofstream ofs( "ins2.pddl" );
	for( int l = 0; l<  int( output.size() ); l++ ){
		ofs << output[ l ] << endl;
	}
	ofs.close();


}
