1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 11707
parse actions...
Number of actions: 97822
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 7210
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
6551 variables of 11707 necessary
0 of 0 mutex groups necessary.
96808 of 97822 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 13102
Preprocessor derived variables: 0
Preprocessor task size: 2218184
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=8.84s]
building causal graph...done! [t=10.70s]
packing state variables...done! [t=10.70s]
Variables: 6551
Facts: 13102
Bytes per state: 820
done initalizing global data [t=10.70s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 1034686 unary operators... done! [789604 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=11.52s, 453224 KB]
Best heuristic value: 5 [g=64, 457 evaluated, 455 expanded, t=32.92s, 453224 KB]
Best heuristic value: 4 [g=65, 458 evaluated, 456 expanded, t=32.96s, 453224 KB]
Best heuristic value: 3 [g=66, 459 evaluated, 457 expanded, t=33.00s, 453224 KB]
Best heuristic value: 2 [g=67, 460 evaluated, 458 expanded, t=33.04s, 453224 KB]
Best heuristic value: 1 [g=68, 461 evaluated, 459 expanded, t=33.08s, 453224 KB]
Solution found!
Actual search time: 22.38s [t=33.08s]
to-c222-c220-W-o2-c233-c232 (1)
to-c220-c218-W-o2-c232-c229 (1)
to-c218-c216-W-o2-c229-c779 (1)
to-c216-c214-W-o2-c779-c778 (1)
to-c214-c212-W-o2-c778-c777 (1)
to-c212-c210-W-o2-c777-c776 (1)
to-c210-c208-W-o2-c776-c775 (1)
to-c208-c206-W-o2-c775-c774 (1)
to-c206-c204-W-o2-c774-c773 (1)
to-c204-c202-W-o2-c773-c772 (1)
to-c202-c200-W-o2-c772-c771 (1)
to-c200-c197-W-o2-c771-c770 (1)
to-c197-c194-W-o2-c770-c769 (1)
to-c194-c191-W-o2-c769-c768 (1)
p-o2 (1)
tr-c191-S-c240 (1)
tr-c240-S-c253 (1)
tr-c253-S-c266 (1)
rr-c266-CCW-c392 (1)
tr-c392-SE-c404 (1)
tr-c404-E-c406 (1)
g-c406-o2-c768 (1)
to-c406-c398-N-o2-c768-c767 (1)
to-c398-c396-W-o2-c767-c766 (1)
to-c396-c392-W-o2-c766-c198 (1)
to-c392-c400-S-o2-c198-c243 (1)
to-c400-c408-S-o2-c243-c256 (1)
ro-c408-c290-CCW-o2-c256-c189 (1)
p-o2 (1)
rr-c290-CW-c408 (1)
rr-c408-CW-c292 (1)
tr-c292-S-c305 (1)
rr-c305-CCW-c412 (1)
rr-c412-CCW-c303 (1)
g-c303-o2-c189 (1)
ro-c303-c414-CCW-o2-c189-c261 (1)
p-o2 (1)
tr-c414-N-c410 (1)
tr-c410-N-c402 (1)
tr-c402-N-c394 (1)
rr-c394-CW-c264 (1)
g-c264-o0-c669 (1)
ro-c264-c394-CCW-o0-c669-c186 (1)
p-o0 (1)
rr-c394-CW-c264 (1)
tr-c264-N-c251 (1)
tr-c251-N-c238 (1)
tr-c238-NE-c192 (1)
rr-c192-CW-c356 (1)
g-c356-o4-c672 (1)
to-c356-c358-E-o4-c672-c673 (1)
to-c358-c360-E-o4-c673-c674 (1)
p-o4 (1)
rr-c360-CW-c200 (1)
tr-c200-E-c202 (1)
tr-c202-E-c204 (1)
tr-c204-E-c206 (1)
tr-c206-E-c208 (1)
tr-c208-E-c210 (1)
tr-c210-E-c212 (1)
tr-c212-E-c214 (1)
tr-c214-E-c216 (1)
tr-c216-E-c218 (1)
tr-c218-E-c220 (1)
tr-c220-E-c222 (1)
tr-c222-E-c224 (1)
tr-c224-E-c226 (1)
tr-c226-E-c228 (1)
dummy-action-228 (1)
Plan length: 69 step(s).
Plan cost: 69
Initial state h value: 6.
Expanded 460 state(s).
Reopened 0 state(s).
Evaluated 462 state(s).
Evaluations: 462
Generated 1576 state(s).
Dead ends: 1 state(s).
Search time: 22.38s
Total time: 33.08s
Solution found.
Peak memory: 453224 KB

Plans and cost: 

