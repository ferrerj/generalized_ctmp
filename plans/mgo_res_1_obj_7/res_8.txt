1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2355
parse actions...
Number of actions: 12105
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1350
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1551 variables of 2355 necessary
0 of 0 mutex groups necessary.
11842 of 12105 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 3102
Preprocessor derived variables: 0
Preprocessor task size: 130051
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.42s]
building causal graph...done! [t=0.48s]
packing state variables...done! [t=0.48s]
Variables: 1551
Facts: 3102
Bytes per state: 196
done initalizing global data [t=0.48s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 58623 unary operators... done! [42659 unary operators]
Best heuristic value: 4 [g=0, 1 evaluated, 0 expanded, t=0.56s, 25684 KB]
Best heuristic value: 3 [g=2, 6 evaluated, 5 expanded, t=0.56s, 25684 KB]
Best heuristic value: 2 [g=3, 7 evaluated, 6 expanded, t=0.56s, 25684 KB]
Best heuristic value: 1 [g=4, 8 evaluated, 7 expanded, t=0.56s, 25684 KB]
Solution found!
Actual search time: 0.08s [t=0.56s]
ro-c50-c109-CW-o2-c54-c60 (1)
p-o2 (1)
rr-c109-CCW-c50 (1)
tr-c50-E-c53 (1)
dummy-action-53 (1)
Plan length: 5 step(s).
Plan cost: 5
Initial state h value: 4.
Expanded 8 state(s).
Reopened 0 state(s).
Evaluated 9 state(s).
Evaluations: 9
Generated 24 state(s).
Dead ends: 0 state(s).
Search time: 0.08s
Total time: 0.56s
Solution found.
Peak memory: 25684 KB

Plans and cost: 

