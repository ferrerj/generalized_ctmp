1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 11707
parse actions...
Number of actions: 97822
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 7210
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
6551 variables of 11707 necessary
0 of 0 mutex groups necessary.
96804 of 97822 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 13102
Preprocessor derived variables: 0
Preprocessor task size: 2218172
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=8.82s]
building causal graph...done! [t=10.70s]
packing state variables...done! [t=10.70s]
Variables: 6551
Facts: 13102
Bytes per state: 820
done initalizing global data [t=10.70s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 1034682 unary operators... done! [789600 unary operators]
Switch from bucket-based to heap-based queue at key = 15285, num_pushes = 9528
Best heuristic value: 39 [g=0, 1 evaluated, 0 expanded, t=11.52s, 453224 KB]
Best heuristic value: 38 [g=1, 2 evaluated, 1 expanded, t=11.60s, 453224 KB]
Best heuristic value: 37 [g=2, 3 evaluated, 2 expanded, t=11.66s, 453224 KB]
Best heuristic value: 36 [g=3, 4 evaluated, 3 expanded, t=11.72s, 453224 KB]
Best heuristic value: 35 [g=4, 5 evaluated, 4 expanded, t=11.78s, 453224 KB]
Best heuristic value: 34 [g=5, 6 evaluated, 5 expanded, t=11.86s, 453224 KB]
Best heuristic value: 33 [g=6, 7 evaluated, 6 expanded, t=11.92s, 453224 KB]
Best heuristic value: 32 [g=7, 8 evaluated, 7 expanded, t=11.98s, 453224 KB]
Best heuristic value: 31 [g=8, 9 evaluated, 8 expanded, t=12.04s, 453224 KB]
Best heuristic value: 30 [g=9, 10 evaluated, 9 expanded, t=12.10s, 453224 KB]
Best heuristic value: 29 [g=10, 11 evaluated, 10 expanded, t=12.18s, 453224 KB]
Best heuristic value: 28 [g=11, 12 evaluated, 11 expanded, t=12.24s, 453224 KB]
Best heuristic value: 27 [g=12, 13 evaluated, 12 expanded, t=12.30s, 453224 KB]
Best heuristic value: 26 [g=13, 14 evaluated, 13 expanded, t=12.36s, 453224 KB]
Best heuristic value: 25 [g=14, 15 evaluated, 14 expanded, t=12.44s, 453224 KB]
Best heuristic value: 24 [g=16, 17 evaluated, 16 expanded, t=12.56s, 453224 KB]
Best heuristic value: 23 [g=17, 18 evaluated, 17 expanded, t=12.62s, 453224 KB]
Best heuristic value: 22 [g=18, 19 evaluated, 18 expanded, t=12.68s, 453224 KB]
Best heuristic value: 21 [g=19, 20 evaluated, 19 expanded, t=12.76s, 453224 KB]
Best heuristic value: 20 [g=20, 21 evaluated, 20 expanded, t=12.82s, 453224 KB]
Best heuristic value: 19 [g=21, 22 evaluated, 21 expanded, t=12.88s, 453224 KB]
Best heuristic value: 18 [g=22, 23 evaluated, 22 expanded, t=12.94s, 453224 KB]
Best heuristic value: 17 [g=23, 24 evaluated, 23 expanded, t=13.00s, 453224 KB]
Best heuristic value: 16 [g=25, 26 evaluated, 25 expanded, t=13.12s, 453224 KB]
Best heuristic value: 15 [g=26, 27 evaluated, 26 expanded, t=13.18s, 453224 KB]
Best heuristic value: 14 [g=27, 28 evaluated, 27 expanded, t=13.24s, 453224 KB]
Best heuristic value: 13 [g=28, 29 evaluated, 28 expanded, t=13.30s, 453224 KB]
Best heuristic value: 12 [g=31, 37 evaluated, 36 expanded, t=13.78s, 453224 KB]
Best heuristic value: 11 [g=34, 47 evaluated, 46 expanded, t=14.38s, 453224 KB]
Best heuristic value: 10 [g=35, 48 evaluated, 47 expanded, t=14.44s, 453224 KB]
Best heuristic value: 9 [g=36, 49 evaluated, 48 expanded, t=14.50s, 453224 KB]
Best heuristic value: 8 [g=37, 50 evaluated, 49 expanded, t=14.56s, 453224 KB]
Best heuristic value: 7 [g=38, 51 evaluated, 50 expanded, t=14.60s, 453224 KB]
Best heuristic value: 6 [g=40, 53 evaluated, 52 expanded, t=14.70s, 453224 KB]
Best heuristic value: 5 [g=69, 323 evaluated, 322 expanded, t=29.82s, 453224 KB]
Best heuristic value: 4 [g=75, 346 evaluated, 345 expanded, t=30.96s, 453224 KB]
Best heuristic value: 3 [g=76, 347 evaluated, 346 expanded, t=31.00s, 453224 KB]
Best heuristic value: 2 [g=77, 348 evaluated, 347 expanded, t=31.06s, 453224 KB]
Best heuristic value: 1 [g=78, 349 evaluated, 348 expanded, t=31.10s, 453224 KB]
Solution found!
Actual search time: 20.40s [t=31.10s]
tr-c228-W-c226 (1)
tr-c226-W-c224 (1)
tr-c224-W-c222 (1)
tr-c222-W-c220 (1)
tr-c220-W-c218 (1)
tr-c218-W-c216 (1)
tr-c216-W-c214 (1)
tr-c214-W-c212 (1)
tr-c212-W-c210 (1)
tr-c210-W-c208 (1)
tr-c208-W-c206 (1)
tr-c206-W-c204 (1)
tr-c204-W-c202 (1)
tr-c202-W-c200 (1)
rr-c200-CCW-c360 (1)
g-c360-o4-c674 (1)
ro-c360-c198-CCW-o4-c674-c43 (1)
p-o4 (1)
rr-c198-CW-c360 (1)
rr-c360-CW-c200 (1)
tr-c200-N-c154 (1)
tr-c154-N-c113 (1)
tr-c113-N-c69 (1)
g-c69-o1-c677 (1)
to-c69-c71-E-o1-c677-c678 (1)
to-c71-c73-E-o1-c678-c679 (1)
to-c73-c75-E-o1-c679-c680 (1)
to-c75-c77-E-o1-c680-c681 (1)
to-c77-c79-E-o1-c681-c682 (1)
to-c79-c125-SE-o1-c682-c713 (1)
to-c125-c164-SW-o1-c713-c747 (1)
to-c164-c166-E-o1-c747-c748 (1)
to-c166-c214-SE-o1-c748-c777 (1)
to-c214-c216-E-o1-c777-c778 (1)
p-o1 (1)
tr-c216-N-c170 (1)
tr-c170-N-c129 (1)
tr-c129-NE-c87 (1)
g-c87-o5-c101 (1)
to-c87-c89-E-o5-c101-c683 (1)
to-c89-c91-E-o5-c683-c686 (1)
to-c91-c94-E-o5-c686-c689 (1)
p-o5 (1)
tr-c94-W-c91 (1)
tr-c91-W-c89 (1)
tr-c89-W-c87 (1)
tr-c87-W-c85 (1)
tr-c85-SW-c127 (1)
tr-c127-SW-c166 (1)
tr-c166-SE-c214 (1)
g-c214-o1-c778 (1)
to-c214-c216-E-o1-c778-c779 (1)
to-c216-c218-E-o1-c779-c229 (1)
to-c218-c220-E-o1-c229-c232 (1)
to-c220-c222-E-o1-c232-c233 (1)
to-c222-c224-E-o1-c233-c234 (1)
p-o1 (1)
tr-c224-NW-c176 (1)
tr-c176-S-c222 (1)
tr-c222-NW-c174 (1)
tr-c174-NE-c135 (1)
tr-c135-NE-c94 (1)
g-c94-o5-c689 (1)
to-c94-c91-W-o5-c689-c686 (1)
to-c91-c89-W-o5-c686-c683 (1)
to-c89-c87-W-o5-c683-c101 (1)
to-c87-c131-S-o5-c101-c142 (1)
to-c131-c89-NE-o5-c142-c101 (1)
to-c89-c133-S-o5-c101-c142 (1)
to-c133-c172-SW-o5-c142-c183 (1)
to-c172-c220-SE-o5-c183-c229 (1)
p-o5 (1)
tr-c220-N-c174 (1)
tr-c174-N-c133 (1)
tr-c133-NE-c91 (1)
tr-c91-E-c94 (1)
tr-c94-E-c97 (1)
tr-c97-E-c100 (1)
dummy-action-100 (1)
Plan length: 79 step(s).
Plan cost: 79
Initial state h value: 39.
Expanded 349 state(s).
Reopened 0 state(s).
Evaluated 350 state(s).
Evaluations: 350
Generated 1491 state(s).
Dead ends: 0 state(s).
Search time: 20.40s
Total time: 31.10s
Solution found.
Peak memory: 453224 KB

Plans and cost: 

