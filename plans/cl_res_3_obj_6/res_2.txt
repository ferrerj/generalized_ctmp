1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6880
parse actions...
Number of actions: 65833
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 4410
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
3864 variables of 6880 necessary
0 of 0 mutex groups necessary.
65069 of 65833 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 7728
Preprocessor derived variables: 0
Preprocessor task size: 1624428
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=6.20s]
building causal graph...done! [t=7.56s]
packing state variables...done! [t=7.56s]
Variables: 3864
Facts: 7728
Bytes per state: 484
done initalizing global data [t=7.56s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 815101 unary operators... done! [521491 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=8.10s, 318168 KB]
Best heuristic value: 5 [g=1, 2 evaluated, 1 expanded, t=8.12s, 318168 KB]
Best heuristic value: 4 [g=2, 3 evaluated, 2 expanded, t=8.14s, 318168 KB]
Best heuristic value: 3 [g=3, 4 evaluated, 3 expanded, t=8.16s, 318168 KB]
Best heuristic value: 2 [g=4, 5 evaluated, 4 expanded, t=8.18s, 318168 KB]
Best heuristic value: 1 [g=5, 6 evaluated, 5 expanded, t=8.20s, 318168 KB]
Solution found!
Actual search time: 0.64s [t=8.20s]
rr-c201-CW-c368 (1)
rr-c368-CW-c203 (1)
tr-c203-N-c194 (1)
tr-c194-N-c185 (1)
tr-c185-N-c176 (1)
dummy-action-176 (1)
Plan length: 6 step(s).
Plan cost: 6
Initial state h value: 6.
Expanded 6 state(s).
Reopened 0 state(s).
Evaluated 7 state(s).
Evaluations: 7
Generated 27 state(s).
Dead ends: 0 state(s).
Search time: 0.64s
Total time: 8.20s
Solution found.
Peak memory: 318168 KB

Plans and cost: 

