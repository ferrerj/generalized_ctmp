1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1091
parse actions...
Number of actions: 10853
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 772
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
661 variables of 1091 necessary
0 of 0 mutex groups necessary.
10845 of 10853 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1322
Preprocessor derived variables: 0
Preprocessor task size: 128151
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.40s]
building causal graph...done! [t=0.44s]
packing state variables...done! [t=0.44s]
Variables: 661
Facts: 1322
Bytes per state: 84
done initalizing global data [t=0.44s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 60821 unary operators... done! [42008 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=0.50s, 24544 KB]
Best heuristic value: 5 [g=1, 3 evaluated, 2 expanded, t=0.52s, 24544 KB]
Best heuristic value: 4 [g=2, 4 evaluated, 3 expanded, t=0.52s, 24544 KB]
Best heuristic value: 3 [g=3, 5 evaluated, 4 expanded, t=0.52s, 24544 KB]
Best heuristic value: 2 [g=4, 6 evaluated, 5 expanded, t=0.52s, 24544 KB]
Best heuristic value: 1 [g=5, 7 evaluated, 6 expanded, t=0.52s, 24544 KB]
Solution found!
Actual search time: 0.08s [t=0.52s]
p-o1 (1)
tr-c17-S-c37 (1)
tr-c37-SE-c63 (1)
tr-c63-SW-c80 (1)
g-c80-o2-c203 (1)
dummy-grasping-o2-action (1)
Plan length: 6 step(s).
Plan cost: 6
Initial state h value: 6.
Expanded 7 state(s).
Reopened 0 state(s).
Evaluated 8 state(s).
Evaluations: 8
Generated 55 state(s).
Dead ends: 0 state(s).
Search time: 0.08s
Total time: 0.52s
Solution found.
Peak memory: 24544 KB

Plans and cost: 

