1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6447
parse actions...
Number of actions: 45302
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 4095
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
3411 variables of 6447 necessary
0 of 0 mutex groups necessary.
44288 of 45302 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 6822
Preprocessor derived variables: 0
Preprocessor task size: 1003704
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=4.04s]
building causal graph...done! [t=5.00s]
packing state variables...done! [t=5.00s]
Variables: 3411
Facts: 6822
Bytes per state: 428
done initalizing global data [t=5.00s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 462366 unary operators... done! [353364 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=5.34s, 212552 KB]
Best heuristic value: 5 [g=6, 10 evaluated, 9 expanded, t=5.50s, 212552 KB]
Best heuristic value: 4 [g=7, 11 evaluated, 10 expanded, t=5.52s, 212552 KB]
Best heuristic value: 3 [g=8, 12 evaluated, 11 expanded, t=5.52s, 212552 KB]
Best heuristic value: 2 [g=9, 13 evaluated, 12 expanded, t=5.54s, 212552 KB]
Best heuristic value: 1 [g=10, 14 evaluated, 13 expanded, t=5.56s, 212552 KB]
Solution found!
Actual search time: 0.56s [t=5.56s]
to-c222-c224-E-o2-c233-c234 (1)
p-o2 (1)
tr-c224-N-c178 (1)
tr-c178-N-c137 (1)
tr-c137-NE-c97 (1)
tr-c97-E-c100 (1)
tr-c100-E-c103 (1)
rr-c103-CW-c345 (1)
tr-c345-S-c347 (1)
tr-c347-S-c349 (1)
dummy-action-349 (1)
Plan length: 11 step(s).
Plan cost: 11
Initial state h value: 6.
Expanded 14 state(s).
Reopened 0 state(s).
Evaluated 15 state(s).
Evaluations: 15
Generated 37 state(s).
Dead ends: 0 state(s).
Search time: 0.56s
Total time: 5.56s
Solution found.
Peak memory: 212552 KB

Plans and cost: 

