1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2899
parse actions...
Number of actions: 15339
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1648
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1942 variables of 2899 necessary
0 of 0 mutex groups necessary.
15076 of 15339 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 3884
Preprocessor derived variables: 0
Preprocessor task size: 166008
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.56s]
building causal graph...done! [t=0.64s]
packing state variables...done! [t=0.64s]
Variables: 1942
Facts: 3884
Bytes per state: 244
done initalizing global data [t=0.64s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 75075 unary operators... done! [54593 unary operators]
Best heuristic value: 4 [g=0, 1 evaluated, 0 expanded, t=0.76s, 32148 KB]
Best heuristic value: 3 [g=1, 3 evaluated, 2 expanded, t=0.76s, 32148 KB]
Best heuristic value: 2 [g=2, 4 evaluated, 3 expanded, t=0.76s, 32148 KB]
Best heuristic value: 1 [g=3, 5 evaluated, 4 expanded, t=0.76s, 32148 KB]
Solution found!
Actual search time: 0.12s [t=0.76s]
p-o4 (1)
rr-c40-CW-c106 (1)
tr-c106-E-c108 (1)
dummy-action-108 (1)
Plan length: 4 step(s).
Plan cost: 4
Initial state h value: 4.
Expanded 5 state(s).
Reopened 0 state(s).
Evaluated 6 state(s).
Evaluations: 6
Generated 22 state(s).
Dead ends: 0 state(s).
Search time: 0.12s
Total time: 0.76s
Solution found.
Peak memory: 32148 KB

Plans and cost: 

