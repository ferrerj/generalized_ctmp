1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6067
parse actions...
Number of actions: 55381
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 3937
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
3332 variables of 6067 necessary
0 of 0 mutex groups necessary.
54611 of 55381 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 6664
Preprocessor derived variables: 0
Preprocessor task size: 1359164
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=5.08s]
building causal graph...done! [t=6.18s]
packing state variables...done! [t=6.18s]
Variables: 3332
Facts: 6664
Bytes per state: 420
done initalizing global data [t=6.18s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 680023 unary operators... done! [435301 unary operators]
Best heuristic value: 3 [g=0, 1 evaluated, 0 expanded, t=6.62s, 273852 KB]
Best heuristic value: 2 [g=1, 2 evaluated, 1 expanded, t=6.64s, 273852 KB]
Best heuristic value: 1 [g=2, 3 evaluated, 2 expanded, t=6.66s, 273852 KB]
Solution found!
Actual search time: 0.48s [t=6.66s]
tr-c219-NE-c213 (1)
tr-c213-NW-c201 (1)
dummy-action-201 (1)
Plan length: 3 step(s).
Plan cost: 3
Initial state h value: 3.
Expanded 3 state(s).
Reopened 0 state(s).
Evaluated 4 state(s).
Evaluations: 4
Generated 26 state(s).
Dead ends: 0 state(s).
Search time: 0.48s
Total time: 6.66s
Solution found.
Peak memory: 273852 KB

Plans and cost: 

