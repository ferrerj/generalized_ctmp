1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2899
parse actions...
Number of actions: 15339
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1648
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1941 variables of 2899 necessary
0 of 0 mutex groups necessary.
15076 of 15339 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 3882
Preprocessor derived variables: 0
Preprocessor task size: 166005
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.56s]
building causal graph...done! [t=0.64s]
packing state variables...done! [t=0.64s]
Variables: 1941
Facts: 3882
Bytes per state: 244
done initalizing global data [t=0.64s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 75075 unary operators... done! [54593 unary operators]
Best heuristic value: 9 [g=0, 1 evaluated, 0 expanded, t=0.74s, 32140 KB]
Best heuristic value: 8 [g=1, 2 evaluated, 1 expanded, t=0.74s, 32140 KB]
Best heuristic value: 7 [g=2, 3 evaluated, 2 expanded, t=0.74s, 32140 KB]
Best heuristic value: 5 [g=4, 5 evaluated, 4 expanded, t=0.74s, 32140 KB]
Best heuristic value: 4 [g=5, 6 evaluated, 5 expanded, t=0.74s, 32140 KB]
Best heuristic value: 3 [g=6, 7 evaluated, 6 expanded, t=0.74s, 32140 KB]
Best heuristic value: 2 [g=7, 8 evaluated, 7 expanded, t=0.76s, 32140 KB]
Best heuristic value: 1 [g=8, 9 evaluated, 8 expanded, t=0.76s, 32140 KB]
Solution found!
Actual search time: 0.12s [t=0.76s]
tr-c37-S-c56 (1)
tr-c56-SE-c66 (1)
g-c66-o0-c40 (1)
to-c66-c63-W-o0-c40-c37 (1)
p-o0 (1)
tr-c63-NE-c59 (1)
g-c59-o4-c21 (1)
to-c59-c40-N-o4-c21-c15 (1)
dummy-action-40 (1)
Plan length: 9 step(s).
Plan cost: 9
Initial state h value: 9.
Expanded 9 state(s).
Reopened 0 state(s).
Evaluated 10 state(s).
Evaluations: 10
Generated 50 state(s).
Dead ends: 0 state(s).
Search time: 0.12s
Total time: 0.76s
Solution found.
Peak memory: 32140 KB

Plans and cost: 

