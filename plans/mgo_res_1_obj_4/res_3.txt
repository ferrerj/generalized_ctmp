1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1539
parse actions...
Number of actions: 7254
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 903
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
966 variables of 1539 necessary
0 of 0 mutex groups necessary.
6991 of 7254 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1932
Preprocessor derived variables: 0
Preprocessor task size: 76120
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.24s]
building causal graph...done! [t=0.26s]
packing state variables...done! [t=0.28s]
Variables: 966
Facts: 1932
Bytes per state: 124
done initalizing global data [t=0.28s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 33945 unary operators... done! [24758 unary operators]
Best heuristic value: 6 [g=0, 1 evaluated, 0 expanded, t=0.32s, 16696 KB]
Best heuristic value: 5 [g=1, 2 evaluated, 1 expanded, t=0.32s, 16696 KB]
Best heuristic value: 4 [g=2, 3 evaluated, 2 expanded, t=0.32s, 16696 KB]
Best heuristic value: 3 [g=3, 4 evaluated, 3 expanded, t=0.32s, 16696 KB]
Best heuristic value: 2 [g=4, 5 evaluated, 4 expanded, t=0.32s, 16696 KB]
Best heuristic value: 1 [g=5, 6 evaluated, 5 expanded, t=0.32s, 16696 KB]
Solution found!
Actual search time: 0.04s [t=0.32s]
tr-c37-S-c56 (1)
tr-c56-SE-c66 (1)
g-c66-o0-c40 (1)
to-c66-c59-N-o0-c40-c21 (1)
to-c59-c40-N-o0-c21-c15 (1)
dummy-action-40 (1)
Plan length: 6 step(s).
Plan cost: 6
Initial state h value: 6.
Expanded 6 state(s).
Reopened 0 state(s).
Evaluated 7 state(s).
Evaluations: 7
Generated 34 state(s).
Dead ends: 0 state(s).
Search time: 0.04s
Total time: 0.32s
Solution found.
Peak memory: 16696 KB

Plans and cost: 

