1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1091
parse actions...
Number of actions: 10853
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 772
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
661 variables of 1091 necessary
0 of 0 mutex groups necessary.
10850 of 10853 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1322
Preprocessor derived variables: 0
Preprocessor task size: 130962
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.42s]
building causal graph...done! [t=0.48s]
packing state variables...done! [t=0.48s]
Variables: 661
Facts: 1322
Bytes per state: 84
done initalizing global data [t=0.48s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 63622 unary operators... done! [43702 unary operators]
Best heuristic value: 39 [g=0, 1 evaluated, 0 expanded, t=0.56s, 25016 KB]
Best heuristic value: 38 [g=1, 3 evaluated, 2 expanded, t=0.56s, 25016 KB]
Best heuristic value: 37 [g=2, 4 evaluated, 3 expanded, t=0.56s, 25016 KB]
Best heuristic value: 36 [g=3, 10 evaluated, 9 expanded, t=0.56s, 25016 KB]
Best heuristic value: 33 [g=4, 13 evaluated, 12 expanded, t=0.56s, 25016 KB]
Best heuristic value: 32 [g=5, 15 evaluated, 14 expanded, t=0.56s, 25016 KB]
Best heuristic value: 31 [g=7, 21 evaluated, 20 expanded, t=0.56s, 25016 KB]
Best heuristic value: 29 [g=7, 24 evaluated, 23 expanded, t=0.58s, 25016 KB]
Best heuristic value: 27 [g=11, 51 evaluated, 50 expanded, t=0.60s, 25016 KB]
Best heuristic value: 26 [g=65, 1981 evaluated, 1980 expanded, t=2.30s, 25016 KB]
Best heuristic value: 25 [g=69, 1994 evaluated, 1993 expanded, t=2.30s, 25016 KB]
Best heuristic value: 23 [g=67, 2012 evaluated, 2011 expanded, t=2.32s, 25016 KB]
Best heuristic value: 22 [g=72, 2049 evaluated, 2048 expanded, t=2.36s, 25016 KB]
Best heuristic value: 18 [g=77, 3528 evaluated, 3527 expanded, t=3.62s, 25016 KB]
Best heuristic value: 15 [g=78, 3529 evaluated, 3528 expanded, t=3.62s, 25016 KB]
Best heuristic value: 12 [g=79, 3530 evaluated, 3529 expanded, t=3.62s, 25016 KB]
Best heuristic value: 10 [g=80, 3531 evaluated, 3530 expanded, t=3.62s, 25016 KB]
Best heuristic value: 9 [g=86, 3544 evaluated, 3543 expanded, t=3.62s, 25016 KB]
Best heuristic value: 8 [g=89, 3547 evaluated, 3546 expanded, t=3.62s, 25016 KB]
Best heuristic value: 7 [g=90, 3548 evaluated, 3547 expanded, t=3.62s, 25016 KB]
Best heuristic value: 3 [g=91, 3551 evaluated, 3550 expanded, t=3.64s, 25016 KB]
Best heuristic value: 2 [g=92, 3552 evaluated, 3551 expanded, t=3.64s, 25016 KB]
Best heuristic value: 1 [g=93, 3553 evaluated, 3552 expanded, t=3.64s, 25016 KB]
Solution found!
Actual search time: 3.16s [t=3.64s]
tr-c85-NE-c72 (1)
tr-c72-NE-c52 (1)
tr-c52-N-c29 (1)
g-c29-o1-c1 (1)
to-c29-c32-E-o1-c1-c177 (1)
ro-c32-c113-CW-o1-c177-c183 (1)
ro-c113-c34-CW-o1-c183-c38 (1)
p-o1 (1)
tr-c34-SW-c54 (1)
tr-c54-SW-c74 (1)
tr-c74-E-c76 (1)
tr-c76-E-c78 (1)
tr-c78-E-c80 (1)
g-c80-o2-c203 (1)
to-c80-c78-W-o2-c203-c202 (1)
to-c78-c76-W-o2-c202-c201 (1)
ro-c76-c163-CCW-o2-c201-c55 (1)
to-c163-c161-W-o2-c55-c52 (1)
ro-c161-c74-CW-o2-c52-c200 (1)
to-c74-c51-N-o2-c200-c55 (1)
ro-c51-c133-CCW-o2-c55-c30 (1)
p-o2 (1)
tr-c133-SE-c163 (1)
tr-c163-NE-c141 (1)
tr-c141-E-c145 (1)
g-c145-o1-c38 (1)
to-c145-c141-W-o1-c38-c35 (1)
p-o1 (1)
tr-c141-SW-c163 (1)
tr-c163-NW-c133 (1)
g-c133-o2-c30 (1)
ro-c133-c49-CCW-o2-c30-c7 (1)
ro-c49-c133-CW-o2-c7-c29 (1)
to-c133-c137-E-o2-c29-c32 (1)
ro-c137-c52-CCW-o2-c32-c10 (1)
p-o2 (1)
tr-c52-E-c55 (1)
rr-c55-CW-c141 (1)
g-c141-o1-c35 (1)
ro-c141-c55-CCW-o1-c35-c13 (1)
to-c55-c58-E-o1-c13-c183 (1)
to-c58-c61-E-o1-c183-c184 (1)
p-o1 (1)
tr-c61-NW-c35 (1)
tr-c35-SW-c55 (1)
tr-c55-W-c52 (1)
g-c52-o2-c10 (1)
ro-c52-c137-CW-o2-c10-c32 (1)
ro-c137-c54-CW-o2-c32-c58 (1)
to-c54-c31-N-o2-c58-c35 (1)
ro-c31-c109-CCW-o2-c35-c13 (1)
to-c109-c107-W-o2-c13-c10 (1)
ro-c107-c28-CW-o2-c10-c32 (1)
p-o2 (1)
tr-c28-NE-c12 (1)
tr-c12-E-c15 (1)
g-c15-o1-c184 (1)
to-c15-c17-E-o1-c184-c185 (1)
to-c17-c19-E-o1-c185-c186 (1)
dummy-goal-o1-action-4 (1)
p-o1 (1)
tr-c19-W-c17 (1)
tr-c17-W-c15 (1)
tr-c15-W-c12 (1)
tr-c12-SW-c28 (1)
g-c28-o2-c32 (1)
ro-c28-c107-CCW-o2-c32-c10 (1)
to-c107-c109-E-o2-c10-c13 (1)
p-o2 (1)
tr-c109-W-c107 (1)
tr-c107-W-c103 (1)
rr-c103-CCW-c23 (1)
tr-c23-W-c22 (1)
g-c22-o0-c172 (1)
to-c22-c45-S-o0-c172-c4 (1)
to-c45-c46-E-o0-c4-c182 (1)
to-c46-c49-E-o0-c182-c7 (1)
to-c49-c52-E-o0-c7-c10 (1)
ro-c52-c137-CW-o0-c10-c32 (1)
to-c137-c141-E-o0-c32-c35 (1)
to-c141-c145-E-o0-c35-c38 (1)
ro-c145-c58-CCW-o0-c38-c183 (1)
to-c58-c61-E-o0-c183-c184 (1)
to-c61-c64-E-o0-c184-c185 (1)
p-o0 (1)
dummy-goal-o0-action-1 (1)
tr-c64-W-c61 (1)
tr-c61-NW-c35 (1)
tr-c35-SW-c55 (1)
g-c55-o2-c13 (1)
to-c55-c58-E-o2-c13-c183 (1)
ro-c58-c145-CW-o2-c183-c38 (1)
to-c145-c149-E-o2-c38-c41 (1)
dummy-goal-o2-action-2 (1)
Plan length: 94 step(s).
Plan cost: 94
Initial state h value: 39.
Expanded 3553 state(s).
Reopened 0 state(s).
Evaluated 3554 state(s).
Evaluations: 3554
Generated 28407 state(s).
Dead ends: 0 state(s).
Search time: 3.16s
Total time: 3.64s
Solution found.
