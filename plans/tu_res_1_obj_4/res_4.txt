1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1319
parse actions...
Number of actions: 14108
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 913
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
807 variables of 1319 necessary
0 of 0 mutex groups necessary.
14092 of 14108 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1614
Preprocessor derived variables: 0
Preprocessor task size: 166137
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.60s]
building causal graph...done! [t=0.72s]
packing state variables...done! [t=0.72s]
Variables: 807
Facts: 1614
Bytes per state: 104
done initalizing global data [t=0.72s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 78500 unary operators... done! [54278 unary operators]
Best heuristic value: 11 [g=0, 1 evaluated, 0 expanded, t=0.96s, 30964 KB]
Best heuristic value: 4 [g=1, 2 evaluated, 1 expanded, t=0.96s, 30964 KB]
Best heuristic value: 3 [g=2, 3 evaluated, 2 expanded, t=0.96s, 30964 KB]
Best heuristic value: 2 [g=3, 4 evaluated, 3 expanded, t=0.96s, 30964 KB]
Best heuristic value: 1 [g=4, 5 evaluated, 4 expanded, t=0.96s, 30964 KB]
Solution found!
Actual search time: 0.24s [t=0.96s]
to-c29-c32-E-o1-c1-c177 (1)
ro-c32-c113-CW-o1-c177-c183 (1)
to-c113-c117-E-o1-c183-c184 (1)
to-c117-c121-E-o1-c184-c185 (1)
dummy-goal-o1-action-1 (1)
Plan length: 5 step(s).
Plan cost: 5
Initial state h value: 11.
Expanded 5 state(s).
Reopened 0 state(s).
Evaluated 6 state(s).
Evaluations: 6
Generated 38 state(s).
Dead ends: 0 state(s).
Search time: 0.24s
Total time: 0.96s
Solution found.
Peak memory: 30964 KB

Plans and cost: 

