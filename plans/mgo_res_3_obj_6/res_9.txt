1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 8551
parse actions...
Number of actions: 66310
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5341
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4667 variables of 8551 necessary
0 of 0 mutex groups necessary.
65292 of 66310 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 9334
Preprocessor derived variables: 0
Preprocessor task size: 1489484
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=5.92s]
building causal graph...done! [t=7.14s]
packing state variables...done! [t=7.14s]
Variables: 4667
Facts: 9334
Bytes per state: 584
done initalizing global data [t=7.14s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 691290 unary operators... done! [527856 unary operators]
Switch from bucket-based to heap-based queue at key = 7658, num_pushes = 6782
Best heuristic value: 50 [g=0, 1 evaluated, 0 expanded, t=7.70s, 340168 KB]
Best heuristic value: 47 [g=1, 2 evaluated, 1 expanded, t=7.74s, 340168 KB]
Best heuristic value: 46 [g=2, 3 evaluated, 2 expanded, t=7.78s, 340168 KB]
Best heuristic value: 45 [g=3, 4 evaluated, 3 expanded, t=7.82s, 340168 KB]
Best heuristic value: 44 [g=4, 5 evaluated, 4 expanded, t=7.86s, 340168 KB]
Best heuristic value: 43 [g=5, 6 evaluated, 5 expanded, t=7.90s, 340168 KB]
Best heuristic value: 42 [g=6, 7 evaluated, 6 expanded, t=7.94s, 340168 KB]
Best heuristic value: 41 [g=7, 8 evaluated, 7 expanded, t=7.98s, 340168 KB]
Best heuristic value: 40 [g=8, 9 evaluated, 8 expanded, t=8.02s, 340168 KB]
Best heuristic value: 39 [g=11, 12 evaluated, 11 expanded, t=8.14s, 340168 KB]
Best heuristic value: 38 [g=12, 13 evaluated, 12 expanded, t=8.18s, 340168 KB]
Best heuristic value: 37 [g=13, 14 evaluated, 13 expanded, t=8.22s, 340168 KB]
Best heuristic value: 36 [g=14, 15 evaluated, 14 expanded, t=8.26s, 340168 KB]
Best heuristic value: 32 [g=16, 17 evaluated, 16 expanded, t=8.34s, 340168 KB]
Best heuristic value: 31 [g=19, 20 evaluated, 19 expanded, t=8.48s, 340168 KB]
Best heuristic value: 26 [g=21, 22 evaluated, 21 expanded, t=8.56s, 340168 KB]
Best heuristic value: 25 [g=22, 23 evaluated, 22 expanded, t=8.60s, 340168 KB]
Best heuristic value: 23 [g=24, 27 evaluated, 26 expanded, t=8.76s, 340168 KB]
Best heuristic value: 22 [g=25, 28 evaluated, 27 expanded, t=8.80s, 340168 KB]
Best heuristic value: 21 [g=26, 29 evaluated, 28 expanded, t=8.84s, 340168 KB]
Best heuristic value: 20 [g=27, 30 evaluated, 29 expanded, t=8.88s, 340168 KB]
Best heuristic value: 19 [g=28, 31 evaluated, 30 expanded, t=8.92s, 340168 KB]
Best heuristic value: 18 [g=29, 32 evaluated, 31 expanded, t=8.96s, 340168 KB]
Best heuristic value: 17 [g=30, 33 evaluated, 32 expanded, t=9.00s, 340168 KB]
Best heuristic value: 16 [g=32, 35 evaluated, 34 expanded, t=9.06s, 340168 KB]
Best heuristic value: 15 [g=33, 36 evaluated, 35 expanded, t=9.10s, 340168 KB]
Best heuristic value: 14 [g=34, 37 evaluated, 36 expanded, t=9.14s, 340168 KB]
Best heuristic value: 13 [g=35, 38 evaluated, 37 expanded, t=9.18s, 340168 KB]
Best heuristic value: 12 [g=40, 146 evaluated, 145 expanded, t=13.16s, 340168 KB]
Best heuristic value: 11 [g=43, 154 evaluated, 153 expanded, t=13.46s, 340168 KB]
Best heuristic value: 10 [g=44, 155 evaluated, 154 expanded, t=13.50s, 340168 KB]
Best heuristic value: 9 [g=45, 156 evaluated, 155 expanded, t=13.52s, 340168 KB]
Best heuristic value: 8 [g=46, 157 evaluated, 156 expanded, t=13.56s, 340168 KB]
Best heuristic value: 7 [g=47, 158 evaluated, 157 expanded, t=13.60s, 340168 KB]
Best heuristic value: 6 [g=49, 160 evaluated, 159 expanded, t=13.64s, 340168 KB]
Best heuristic value: 5 [g=53, 1011 evaluated, 1010 expanded, t=45.44s, 340168 KB]
Best heuristic value: 4 [g=54, 1012 evaluated, 1011 expanded, t=45.46s, 340168 KB]
Best heuristic value: 3 [g=55, 1013 evaluated, 1012 expanded, t=45.48s, 340168 KB]
Best heuristic value: 2 [g=56, 1014 evaluated, 1013 expanded, t=45.52s, 340168 KB]
Best heuristic value: 1 [g=57, 1015 evaluated, 1014 expanded, t=45.54s, 340168 KB]
Solution found!
Actual search time: 38.40s [t=45.54s]
tr-c227-W-c225 (1)
tr-c225-W-c223 (1)
tr-c223-W-c221 (1)
tr-c221-W-c219 (1)
tr-c219-W-c217 (1)
tr-c217-W-c215 (1)
tr-c215-W-c213 (1)
tr-c213-W-c211 (1)
tr-c211-W-c209 (1)
tr-c209-W-c207 (1)
tr-c207-W-c205 (1)
tr-c205-W-c203 (1)
tr-c203-W-c201 (1)
tr-c201-W-c199 (1)
tr-c199-W-c196 (1)
tr-c196-W-c193 (1)
tr-c193-W-c190 (1)
rr-c190-CW-c354 (1)
tr-c354-S-c370 (1)
rr-c370-CW-c238 (1)
rr-c238-CW-c368 (1)
g-c368-o4-c672 (1)
to-c368-c352-N-o4-c672-c67 (1)
ro-c352-c191-CW-o4-c67-c768 (1)
ro-c191-c353-CW-o4-c768-c269 (1)
p-o4 (1)
rr-c353-CCW-c191 (1)
tr-c191-E-c194 (1)
tr-c194-E-c197 (1)
tr-c197-NE-c154 (1)
tr-c154-N-c113 (1)
tr-c113-N-c69 (1)
g-c69-o1-c677 (1)
to-c69-c71-E-o1-c677-c678 (1)
to-c71-c73-E-o1-c678-c679 (1)
to-c73-c75-E-o1-c679-c680 (1)
to-c75-c77-E-o1-c680-c681 (1)
to-c77-c79-E-o1-c681-c682 (1)
to-c79-c81-E-o1-c682-c92 (1)
to-c81-c127-SE-o1-c92-c714 (1)
to-c127-c166-SW-o1-c714-c748 (1)
to-c166-c212-S-o1-c748-c777 (1)
to-c212-c214-E-o1-c777-c778 (1)
to-c214-c216-E-o1-c778-c779 (1)
p-o1 (1)
tr-c216-N-c170 (1)
tr-c170-N-c129 (1)
tr-c129-NE-c87 (1)
g-c87-o5-c101 (1)
to-c87-c89-E-o5-c101-c683 (1)
to-c89-c91-E-o5-c683-c686 (1)
ro-c91-c342-CCW-o5-c686-c46 (1)
p-o5 (1)
rr-c342-CW-c91 (1)
tr-c91-E-c94 (1)
tr-c94-E-c97 (1)
tr-c97-E-c100 (1)
dummy-action-100 (1)
Plan length: 58 step(s).
Plan cost: 58
Initial state h value: 50.
Expanded 1015 state(s).
Reopened 0 state(s).
Evaluated 1016 state(s).
Evaluations: 1016
Generated 4701 state(s).
Dead ends: 0 state(s).
Search time: 38.40s
Total time: 45.54s
Solution found.
Peak memory: 340168 KB

Plans and cost: 

