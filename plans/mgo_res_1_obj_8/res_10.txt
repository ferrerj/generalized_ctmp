1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2627
parse actions...
Number of actions: 13722
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1499
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1748 variables of 2627 necessary
0 of 0 mutex groups necessary.
13461 of 13722 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 3496
Preprocessor derived variables: 0
Preprocessor task size: 148040
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.50s]
building causal graph...done! [t=0.56s]
packing state variables...done! [t=0.56s]
Variables: 1748
Facts: 3496
Bytes per state: 220
done initalizing global data [t=0.56s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 66851 unary operators... done! [48628 unary operators]
Best heuristic value: 8 [g=0, 1 evaluated, 0 expanded, t=0.68s, 30020 KB]
Best heuristic value: 7 [g=1, 2 evaluated, 1 expanded, t=0.68s, 30020 KB]
Best heuristic value: 6 [g=2, 3 evaluated, 2 expanded, t=0.68s, 30020 KB]
Best heuristic value: 5 [g=4, 7 evaluated, 6 expanded, t=0.70s, 30020 KB]
Best heuristic value: 4 [g=5, 8 evaluated, 7 expanded, t=0.70s, 30020 KB]
Best heuristic value: 3 [g=6, 9 evaluated, 8 expanded, t=0.70s, 30020 KB]
Best heuristic value: 2 [g=7, 10 evaluated, 9 expanded, t=0.70s, 30020 KB]
Best heuristic value: 1 [g=8, 11 evaluated, 10 expanded, t=0.70s, 30020 KB]
Solution found!
Actual search time: 0.14s [t=0.70s]
tr-c35-W-c32 (1)
rr-c32-CCW-c98 (1)
g-c98-o5-c17 (1)
ro-c98-c30-CCW-o5-c17-c10 (1)
p-o5 (1)
tr-c30-E-c33 (1)
g-c33-o3-c11 (1)
to-c33-c17-N-o3-c11-c155 (1)
dummy-action-17 (1)
Plan length: 9 step(s).
Plan cost: 9
Initial state h value: 8.
Expanded 11 state(s).
Reopened 0 state(s).
Evaluated 12 state(s).
Evaluations: 12
Generated 46 state(s).
Dead ends: 0 state(s).
Search time: 0.14s
Total time: 0.70s
Solution found.
Peak memory: 30020 KB

Plans and cost: 

