1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 9603
parse actions...
Number of actions: 76814
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5964
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
5296 variables of 9603 necessary
0 of 0 mutex groups necessary.
75786 of 76814 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 10592
Preprocessor derived variables: 0
Preprocessor task size: 1732353
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=6.88s]
building causal graph...done! [t=8.36s]
packing state variables...done! [t=8.36s]
Variables: 5296
Facts: 10592
Bytes per state: 664
done initalizing global data [t=8.36s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 805744 unary operators... done! [615094 unary operators]
Best heuristic value: 16 [g=0, 1 evaluated, 0 expanded, t=9.00s, 379924 KB]
Best heuristic value: 15 [g=1, 2 evaluated, 1 expanded, t=9.06s, 379924 KB]
Best heuristic value: 14 [g=2, 3 evaluated, 2 expanded, t=9.10s, 379924 KB]
Best heuristic value: 13 [g=3, 4 evaluated, 3 expanded, t=9.14s, 379924 KB]
Best heuristic value: 12 [g=4, 5 evaluated, 4 expanded, t=9.20s, 379924 KB]
Best heuristic value: 8 [g=5, 6 evaluated, 5 expanded, t=9.24s, 379924 KB]
Best heuristic value: 7 [g=6, 7 evaluated, 6 expanded, t=9.28s, 379924 KB]
Best heuristic value: 6 [g=7, 8 evaluated, 7 expanded, t=9.30s, 379924 KB]
Best heuristic value: 5 [g=8, 9 evaluated, 8 expanded, t=9.34s, 379924 KB]
Best heuristic value: 4 [g=9, 10 evaluated, 9 expanded, t=9.38s, 379924 KB]
Best heuristic value: 3 [g=10, 11 evaluated, 10 expanded, t=9.42s, 379924 KB]
Best heuristic value: 2 [g=11, 12 evaluated, 11 expanded, t=9.44s, 379924 KB]
Best heuristic value: 1 [g=12, 13 evaluated, 12 expanded, t=9.48s, 379924 KB]
Solution found!
Actual search time: 1.12s [t=9.48s]
tr-c100-W-c97 (1)
tr-c97-W-c94 (1)
rr-c94-CCW-c344 (1)
g-c344-o5-c47 (1)
ro-c344-c92-CCW-o5-c47-c24 (1)
p-o5 (1)
tr-c92-E-c95 (1)
tr-c95-E-c98 (1)
tr-c98-E-c101 (1)
g-c101-o3-c27 (1)
to-c101-c59-N-o3-c27-c572 (1)
to-c59-c53-N-o3-c572-c550 (1)
dummy-action-53 (1)
Plan length: 13 step(s).
Plan cost: 13
Initial state h value: 16.
Expanded 13 state(s).
Reopened 0 state(s).
Evaluated 14 state(s).
Evaluations: 14
Generated 55 state(s).
Dead ends: 0 state(s).
Search time: 1.12s
Total time: 9.48s
Solution found.
Peak memory: 379924 KB

Plans and cost: 

