1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1091
parse actions...
Number of actions: 10853
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 772
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
661 variables of 1091 necessary
0 of 0 mutex groups necessary.
10850 of 10853 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1322
Preprocessor derived variables: 0
Preprocessor task size: 130962
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.42s]
building causal graph...done! [t=0.46s]
packing state variables...done! [t=0.48s]
Variables: 661
Facts: 1322
Bytes per state: 84
done initalizing global data [t=0.48s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 63622 unary operators... done! [43702 unary operators]
Best heuristic value: 2 [g=0, 1 evaluated, 0 expanded, t=0.54s, 25016 KB]
Best heuristic value: 1 [g=1, 2 evaluated, 1 expanded, t=0.54s, 25016 KB]
Solution found!
Actual search time: 0.06s [t=0.54s]
to-c80-c60-N-o2-c203-c64 (1)
dummy-goal-o2-action-3 (1)
Plan length: 2 step(s).
Plan cost: 2
Initial state h value: 2.
Expanded 2 state(s).
Reopened 0 state(s).
Evaluated 3 state(s).
Evaluations: 3
Generated 19 state(s).
Dead ends: 0 state(s).
Search time: 0.06s
Total time: 0.54s
Solution found.
Peak memory: 25016 KB

Plans and cost: 

