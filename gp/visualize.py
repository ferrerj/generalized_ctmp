#! /usr/bin/env python
import sys, os, pydot, glob

OUTPUT_DIRNAME = "flows/"


#**************************************#
# MAIN
#**************************************#
try:
   program_dir_name = sys.argv[1]
except:
   print "Usage:"
   print sys.argv[0] + " <program dir>"
   sys.exit(-1)

if (os.path.isdir( OUTPUT_DIRNAME ) == False ):
    cmd = "mkdir " + OUTPUT_DIRNAME 
    print cmd
    os.system(cmd)  

# Splitting the programs
for program_file_name in glob.glob(program_dir_name+"*.txt"):
    programs=[]
    programs.append("")
    program_file = open(program_file_name,'r')    
    for line in program_file:
        if line == "\n":
            programs.append("")
        else:
            programs[len(programs)-1] = programs[len(programs)-1] + line            
    program_file.close()

    i=0
    for p in reversed(programs):
        program_file = open(OUTPUT_DIRNAME+program_file_name[len(program_dir_name):],'w')
        #program_file = open(OUTPUT_DIRNAME+program_file_name[len(program_dir_name):]+"-"+str(i),'w')
        program_file.write(p)
        program_file.close()        
        i=i+1        
        
# Parsing the programs
for program_file_name in glob.glob(OUTPUT_DIRNAME+"/*"):
    print
    print "Generating flow graph for " + program_file_name + "..."
    graph = pydot.Dot(graph_type='digraph')

    nodes = []
    program_file = open(program_file_name,'r')
    i=0
    for line in program_file:
        splitted_line = line.replace(" ","-").replace("INS-","").split("-")        

        #label= str(i)+"."+"-".join(map(str,splitted_line[:-1]))
        label= str(i)+"."
        #label= str(i)+"."+"-".join(map(str,splitted_line[:-2]))       
        
        if "JMP" == splitted_line[0]:
            shape_type = "diamond"
            label = label + "-".join(map(str,splitted_line[-2:]))
        else:
            shape_type = "box"
            label = label + "-".join(map(str,splitted_line[:-1]))

        nodes.append(pydot.Node(label,shape=shape_type))
        graph.add_node(nodes[i])
        i=i+1

    for i in range(0,len(nodes)-1):
        graph.add_edge(pydot.Edge(nodes[i], nodes[i+1]))
        if "JMP-" in nodes[i].get_name():
            target = int(nodes[i].get_name().split("-")[2])
            #target = int(nodes[i].get_name().split("-")[3])
            tailout = "e"
            headout = "n"
            if( i == 0 ):
                tailout="w"
                headout="w"
            elif( i == 2 or i == 4):
                headout="e"
            graph.add_edge(pydot.Edge(nodes[i], nodes[target],tailport=tailout,headport=headout))
            label=nodes[i].get_name().replace('"',"").split("-")
            nodes[i].set_label(str(i)+"."+"-".join(map(str,label[4:])))
            #nodes[i].set_label(str(i)+"."+"-".join(map(str,label[5:])))
                        
    program_file.close()
    graph.write_png(program_file_name.replace(".","-").replace("_","-")+'.png')
#    graph.write_pdf(program_file_name.replace(".","-").replace("_","-")+'.pdf')

   
sys.exit(0)
