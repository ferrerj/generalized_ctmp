1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1091
parse actions...
Number of actions: 10853
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 772
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
660 variables of 1091 necessary
0 of 0 mutex groups necessary.
10839 of 10853 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1320
Preprocessor derived variables: 0
Preprocessor task size: 125333
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.38s]
building causal graph...done! [t=0.42s]
packing state variables...done! [t=0.44s]
Variables: 660
Facts: 1320
Bytes per state: 84
done initalizing global data [t=0.44s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 58019 unary operators... done! [40313 unary operators]
Best heuristic value: 7 [g=0, 1 evaluated, 0 expanded, t=0.50s, 24088 KB]
Best heuristic value: 6 [g=1, 3 evaluated, 2 expanded, t=0.50s, 24088 KB]
Best heuristic value: 5 [g=2, 4 evaluated, 3 expanded, t=0.50s, 24088 KB]
Best heuristic value: 4 [g=3, 5 evaluated, 4 expanded, t=0.50s, 24088 KB]
Best heuristic value: 3 [g=4, 6 evaluated, 5 expanded, t=0.50s, 24088 KB]
Best heuristic value: 2 [g=5, 7 evaluated, 6 expanded, t=0.50s, 24088 KB]
Best heuristic value: 1 [g=6, 8 evaluated, 7 expanded, t=0.50s, 24088 KB]
Solution found!
Actual search time: 0.06s [t=0.50s]
p-o0 (1)
tr-c149-NW-c117 (1)
tr-c117-W-c113 (1)
tr-c113-W-c109 (1)
tr-c109-NW-c93 (1)
g-c93-o1-c1 (1)
dummy-grasping-o1-action (1)
Plan length: 7 step(s).
Plan cost: 7
Initial state h value: 7.
Expanded 8 state(s).
Reopened 0 state(s).
Evaluated 9 state(s).
Evaluations: 9
Generated 68 state(s).
Dead ends: 0 state(s).
Search time: 0.06s
Total time: 0.50s
Solution found.
Peak memory: 24088 KB

Plans and cost: 

