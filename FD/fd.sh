
!#/bin/bash

#$ -l s_vmem=8G

#$ -N "a_job"

PROBLEM="TU_2"
INSTANCE="res_5_obj_3"
ENCODING="strips"
PLANNER="FD_lgbfs"
FOLDER="Planners/benchmarks/icaps/$PROBLEM/$ENCODING/$INSTANCE"
RESULTS="results/icaps/$PROBLEM/$ENCODING/$PLANNER/$INSTANCE.out"

./Planners/FD/plan $FOLDER/domain.pddl $FOLDER/inst_domain.pddl --heuristic "hff=ff()" --search "lazy_greedy(hff, preferred=hff)" > $RESULTS
