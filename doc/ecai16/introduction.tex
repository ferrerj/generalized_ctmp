\section{Introduction}

%(Planning in robotics introduction with references)
Planning in robotics involve a robot that can move in an environment while avoiding collisions, and objects with geometrical dimensions that can be moved by the robot. Consider the example of Figure \ref{fig:init}, a gripper has to grasp the green object, however red object is blocking the trajectory. Thus, planning with robots usually involves a combinations of task and motion planning (CTMP). Symbolic task planning allows to achieve high-level goals, like what object needs to be grasped to clear a path, while motion planning is responsible of geometric reasoning and checks the feasibility of high-level actions [Cambon et al, 2004; Wolfe et al., 2010; Lagriffoul et al., 2012; Lozano-Perez and Kaelbling, 2014]. CTMP problems are not easy to solve because task planners usually doesn't deal with geometric information and identify which high-level preconditions are not satisfied to perform a low-level action is sometimes hard. Also, motion planners alone cannot deal with goals with strong interactions. For this, CTMP problems are usually thought to be outside the scope of standard AI planners.


\begin{figure}
    \centering
    \includegraphics[width=0.2\textwidth]{img/intro_ex.png}
    \caption{Gripper must grasp green object.}
    \label{fig:init}
\end{figure}


%(Integration of task and motion planning)
The aim of this work is to provide an alternative integration of task and motion planning where symbolic and geometric reasoning are addressed in combination, without giving any of these parts the secondary role of verifying action feasibility. Unlike recent works that have the same motivation \cite{garrett2014ffrob}, we present a generalized planning as Domain Control Knowledge (DCK) approach to generate a planning program for the CTMP problem using a set of high-level tasks which are a relaxation of the original CTMP problems. This results in problems that can be solved with classical planners off-the-shelf. For this, we discretize the configuration space and present a formulation of task and motion planning in STRIPS. This encoding allows to compute the \emph{collision-free} configurations and actions. For movable objects, we compute the possible overlaps between pairs of configurations and explicity include them as action preconditions. This is important, as the possible overlaps depend on the discretization of the configuration space. 

% Domain Control Knowledge( DCK )
Domain Control Knowledge (DCK) is a way to express how planning problems must be addressed to solve a given domain. There are many approaches from the last 40 years with macro-actions \cite{Fikes72}, to finite state controllers \cite{Geffner:FSM:AAAI10, Infantes:FSC:ECAI2010, Zilberstein:Gplanning:icaps11,Giacomo:FSM:ICAPS13}. Macro-actions may help to accelerate the search but they are not general for any problem instance. While finite state controllers showed a great potential to produce compact DCK that generalize the solution of a domain. Hierachical Task Networks (HTNs) \cite{Erol96}  or procedural DCK \cite{baier:procedural} are prominent examples about how to control the knowledge of a specific domain.

% Generalized plans in the form of planning programs
In this paper we show how DCK can improve the performance for CTMP problems.
Therefore, generalized planning allows to solve the domain for any instance of CTMP which involves a robot that has to achieve several tasks. The form we express this generalized planning is with planning programs \cite{Jimenez15,segovia16a} that empower the programming of instructions, conditional loops, nested procedures and even recursion.

% Introduce the idea and small example
The usage of planning programs \cite{Jimenez15} as DCK for the CTMP can help to guide the search, in such a way that unsolvable problems by previous approaches \cite{ferrerplanning} now become easier problems.


\begin{figure}[H]
\centering
\begin{small} 
\begin{verbatim} 
0. grasp( ball )     
1. drop( ball , bin )     
2. next-ball( ball , ball' ) 
3. goto( 0 , !( ball == none ) ) 
4. end 
\end{verbatim} 
\end{small} 
\caption{Generalized plan in the form of a {\it planning program} for 2 balls in 2 bins.} \label{fig:program1} 
\end{figure}

The DCK for the CTMP works as a serialization of the goal condition in order to achieve an atomic goal of the real problem. One example can a be a 3 x 3 grid of Figure \ref{fig:2b2b}, where in the bottom-left corner we have a robotic hand, in the bottom-right corner we have a blue and a red ball, and the goal is to place the blue ball into the blue bin and the red ball into the red bin, both of them placed in the top of the grid. In this case, the CTMP has all the possible configurations of the robotic hand in the environment (\textit{9 positions, 8 directions, and 3 hand states} $\{ empty, red, blue\}$). One solution is to move right twice, grasp the red ball, move 2 cells in the top direction, drop the ball in the red bin, move back 2 cells towards bottom, grasp the other ball, move up and left twice and drop the ball in the blue bin. Then, an abstraction of the problem can be to take the balls in some assigned order and drop it in its goal bin. This abstraction can help to produce a general program like grasp the current ball, drop it in its bin, move to the next ball and repeat the process while there are balls to place. So, instead of solving a complex problem with the CTMP, the planning program splits the problem into 4 subtasks which are a serialization of the original CTMP problem: \"grasp blue ball\", \"drop it in blue bin\", \"grasp red ball\" and \"drop it in red bin\".

\begin{figure}[H]
\begin{tiny}
\begin{center}
\begin{tikzpicture}[scale=.7]
  \begin{scope}
    \draw (0, 0) grid (3, 3);
	\filldraw[blue] (2.333,0.5) circle (2pt) ;
	\filldraw[red] (2.666,0.5) circle (2pt) ;
	\draw[blue] (0.5, 2.5) circle(4pt);
	\draw[red] (2.5, 2.5) circle(4pt);
	\node[anchor=center] at (0.5, 0.5) {U};
  \end{scope}
\end{tikzpicture}
\end{center}
\end{tiny}
\caption{Example 2 balls in 2 bins tasks in a $5\times 5$ grid.}
\label{fig:2b2b}
\end{figure}


% Contributions
On the first hand, one contribution of this paper is the proposed integration of task and motion planning into STRIPS, so it can produce solutions using an off-the-shelf classical planner. On second hand, this is the first time to our knowledge generalized planning is applied as DCK for planning in robotics, reducing the complexity and guiding the search towards a final goal for realistic problems.

% Paper organization
This paper is organized as follows: First it introduces classical, motion and generalized planning that are the core to understand the idea. Then, it continues by explaining the kind of generalized planning we are going to use, called planning programs. The idea follows the background with the CTMP model and the connection it has with generalized planning. In order to test the approach we report the experiments finishing with the related work and the conclusions, and also considering future work.



