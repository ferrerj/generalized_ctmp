/****** COMPILER INCLUDES ***********/
#include "include.h"
#include<fstream>

#define INSTR_WITH_PARAMS false

/****** COMPILER DEFINED TYPES ***********/
//typedef std::vector< std::string > StringVec;
typedef std::vector< StringVec > StringDVec;
typedef std::vector< StringDVec > StringTVec;
typedef std::set< std::string > StringSet;

/****** COMPILER GLOBAL VARIABLES ***********/
Domain * d;						// original domain
Domain * cd;					// compiled domain
Instance * cins;				// compiled instance
std::vector< Instance * > ins;	// vector of instances
unsigned bound;					// number of max lines (bound)
unsigned total_instances;		// number of total instances
unsigned total_stack_pred;		// number of total predicates in the stack
unsigned total_stack_vars;		// number of total variables in the recursive calls
unsigned procedures;			// number of procedures
unsigned stack_size;			// max number of procedures in the stack
bool to_program;				// boolean to know if program instructions can be called
StringSet stack_pred;			// stack predicates
StringSet stack_vars;			// variables for recursive calls
StringDVec stack_cons; 			//constants for each stack predicate
std::string goal_pred = "DONE-PROGRAMMING";		// goal predicate
std::string accu_pred = "ACCUMULATOR";			// accumulator predicate
std::string eval_pred = "DONE-EVALUATING";		// evaluating predicate
std::string stack_row_pred = "NEXT-STACK-ROW";	// link between stack rows predicate
std::string stack_top_pred = "TOP-STACK";		// top row of the stack
std::map< std::string , StringVec > act2ins; 	// Variable to map action into programming instructions
StringVec stack_proc;			// Main and procedures in specific stack row
StringVec stack_lines;			// Main/Procedure line in specific stack row
StringVec rows;					// Constant objects ROWs for the stack
IntSet effs; 					// non-static fluents
IntSet unclean_effs; 			// avoid unclean derived predicates effects
StringVec tests;				// tests predicates
StringDVec calls;				// programming calls predicates
StringVec emptys;				// empty lines predicates
StringDVec instr;				// instruction predicates
StringVec endinstr;				// end instructions predicates
StringTVec conds;				// condition predicates
StringTVec jumps;				// jumps predicates
StringVec nojumps;				// no jumps predicates

/****** COMPILER FUNCTIONS ***********/
IntSet preds( Condition * c ) {
	And * a = dynamic_cast< And * >( c );
	Forall * f = dynamic_cast< Forall * >( c );
	Ground * g = dynamic_cast< Ground * >( c );
	Or * o = dynamic_cast< Or * >( c );
	Not * n = dynamic_cast< Not * >( c );
	When * w = dynamic_cast< When * >( c );

	if ( f ) return preds( f->cond );
	if ( n ) return preds( n->cond );
	if ( w ) return preds( w->cond );

	IntSet out;
	if ( g ) out.insert( d->preds.index( g->name ) );
	for ( unsigned i = 0; a && i < a->conds.size(); ++i ) {
		IntSet s = preds( a->conds[i] );
		out.insert( s.begin(), s.end() );
	}
	if ( o ) {
		out = preds( o->first );
		IntSet s = preds( o->second );
		out.insert( s.begin(), s.end() );
	}
	return out;
}

void addStackRow( Condition * c, int par ) {
	And * a = dynamic_cast< And * >( c );
	Forall * f = dynamic_cast< Forall * >( c );
	Ground * g = dynamic_cast< Ground * >( c );
	Or * o = dynamic_cast< Or * >( c );
	Not * n = dynamic_cast< Not * >( c );
	When * w = dynamic_cast< When * >( c );

	if ( f ) addStackRow( f->cond, par );
	if ( n ) addStackRow( n->cond, par );
	if ( w ) addStackRow( w->pars, par );
	if ( w ) addStackRow( w->cond, par );
	if ( g && stack_pred.find( g->name ) != stack_pred.end() )
		g->params.push_back( par );
	for ( unsigned i = 0; a && i < a->conds.size(); ++i )
		addStackRow( a->conds[i], par );
	if ( o ) {
		addStackRow( o->first, par );
		addStackRow( o->second, par );
	}
}

void init(int argc, char *argv[]){
	// check input correctness
	if ( argc < 9 ) {
		std::cout << "Usage: ./bin/compileWhile9 <domain.pddl> <num of task\\s> <task\\s.pddl> <bound> <procedures> <num of predicates to the stack> <predicate\\s> <num of objects to the call> <object\\s> <stack size>\n";
		std::cout << "Writes domain file to standard output and instance file to standard error\n";
		exit( 1 );
	}

	int offset = 1;

	// read input domain
	d = new Domain( argv[ offset++ ] ); 

	// extract the total number of instances
	std::istringstream ti( argv[ offset++ ] );
	ti >> total_instances;
	
	// read input instances
	for(unsigned i = 0 ; i < total_instances; i++){
		ins.push_back( new Instance( *d, argv[ offset++ ] ) );
	}

	// extract max number of lines per procedure and main (bound)
	std::istringstream is( argv[ offset++ ] );
	is >> bound;

	// extract number of procedures
	is.clear();
	is.str( argv[ offset++ ] );	
	is >> procedures;	

	// extract number of predicates in stack
	is.clear();
	is.str( argv[ offset++ ] );
	is >> total_stack_pred;
	
	// read input predicates
	for( unsigned i = 0; i < total_stack_pred; i++ ){
		stack_pred.insert( std::string( argv[ offset++ ] ) );
	}

	// extract variables for recursive calls
	is.clear();
	is.str( argv[ offset++ ] );
	is >> total_stack_vars;

	// read input variables for recursive calls
	for( unsigned i = 0; i < total_stack_vars; i++ ){
		stack_vars.insert( std::string( argv[ offset++ ] ) );

	}
	
	// assigning pile size by default
	is.clear();
	is.str( argv[ offset++ ] );
	is >> stack_size;

	// know if we have to program or not
	is.clear();
	is.str( argv[ offset++ ] );
	is >> to_program;
}

void create_compiled_domain( std::string &name, bool condeffects = true, bool typed = true, bool cons = true, bool costs = true){
	cd = new Domain;
	cd->name = name;
	cd->condeffects = condeffects;
	cd->typed = typed;
	cd->cons = cons;
	cd->costs = costs;
	//cd->equality = d->equality;		
}

void add_types(){ 
	// Add types
	cd->setTypes( d->copyTypes() );
	cd->createType( "STACKROW" );

	// Add objects and constants as compiled constants
	for ( unsigned i = 0; i < cd->types.size(); ++i ) { 
		//BUG of duplicate objects (workaround in the compiler)
		std::set< std::string > scons;
		std::set< std::string > sobj;

		for ( unsigned j = 0; j < cd->types[i]->constants.size(); ++j){
			scons.insert( cd->types[i]->constants[j] ) ;
		}

		for ( unsigned j = 0; j < cd->types[i]->objects.size(); ++j ){
			std::string obj = cd->types[i]->objects[j];
			if ( scons.find( obj ) == scons.end() ) 
				sobj.insert( obj );
		}
		//Insert
		for(std::set<std::string>::iterator it = sobj.begin(); it != sobj.end(); it++){
			cd->types[i]->constants.insert( *it );
		}

		cd->types[i]->objects.tokens.clear();
		cd->types[i]->objects.tokenMap.clear();
		cd->types[i]->objects.types.clear();
	}
	Type *t = cd->types.get( "STACKROW" );
	for (unsigned i = 0; i < stack_size; i++){
		std::stringstream ss;
		ss << "ROW" << i ;
		rows.push_back( ss.str() );
		t->constants.insert( ss.str() );
	}
}

void add_predicates(){
	// Copy predicates
	for ( unsigned i = 0; i < d->preds.size(); ++i ){
		StringVec sv = d->typeList( d->preds[ i ] );
		if( stack_pred.find( d->preds[i]->name ) != stack_pred.end() ){
			//for( int i = 0; i < int(sv.size()); i++ )
			//	std::cout << sv[i] << std::endl;
			sv.push_back( "STACKROW" );
		}
		cd->createPredicate( d->preds[i]->name, sv );
	}

	// Add empty stack predicate
	cd->createPredicate( "EMPTY-STACK" ); 
	
	// Link stack rows
	StringVec params;
	params.push_back( "STACKROW" );
	params.push_back( "STACKROW" );	
	cd->createPredicate( stack_row_pred , params );

	// Add top stack predicate
	params.resize(1);
	cd->createPredicate( stack_top_pred , params );

	// Add predicates for each pair procedure-row in the stack
	for( unsigned p = 0; p < procedures; p++){
		std::stringstream ss;
		ss << "STACK-PROCEDURE-" << p;
		cd->createPredicate( ss.str() , params);
		stack_proc.push_back( ss.str() );
	}
	stack_proc.push_back( "STACK-MAIN" );
	cd->createPredicate( "STACK-MAIN", params );
	
	// Add predicates for each pair line-row in the stack
	for( unsigned l = 0; l <= bound; l++){
		std::stringstream ss;
		ss << "STACK-LINE-" << l;
		cd->createPredicate( ss.str(), params );
		stack_lines.push_back( ss.str() );
	}

	// Add predicates for each test
	for( unsigned k = 0; k < total_instances; k++){
		std::stringstream ss;
		ss << "TEST-" <<k;
		tests.push_back(ss.str());
		cd->createPredicate(tests[k]);
	}

	// Add calls predicates for procedures
	StringVec sv( stack_vars.begin() , stack_vars.end() );
	for(unsigned p = 0; p <= procedures; p++){
		calls.push_back( StringVec() );
		for( unsigned p2 = 0; p2 <= procedures; p2++ ){
			for( unsigned l = 0; l < bound; l++ ){
				std::stringstream ss;
				ss << "CALL-" << p << "-" << p2 << "-" << l;
				calls[ p ].push_back( ss.str() );
				if( p2 >= p )
					cd->createPredicate( calls[ p ][ p2 * bound + l ] , sv);
			}		
		}
	}	

	// Add empty predicates for each pair main-line
	for( unsigned l = 0; l <= bound; l++ ){
		std::stringstream ss;
		ss << "EMPTY-" << l; // Main
		emptys.push_back( ss.str() );
		cd->createPredicate( emptys[ l ] );
	}

	// Add instruction predicates for each pair procedure-line and main-line
	instr.resize( d->actions.size() );
	for ( unsigned i = 0; i < d->actions.size(); ++i )
		for ( unsigned p = 0, current_pos = 0; p <= procedures ; p++){
			for ( unsigned k = 0; k < bound; ++k, current_pos++ ) {
				std::stringstream ss;
				if ( p < procedures ) ss << "INS-" << d->actions[i]->name << "-" << p << "-" << k; // Procedure P
				else ss << "INS-" << d->actions[i]->name << "-" << k; // Main
				instr[i].push_back( ss.str() );
				if( INSTR_WITH_PARAMS ) cd->createPredicate( instr[i][current_pos], d->typeList( d->actions[i] ) );
				else cd->createPredicate( instr[i][current_pos] );
			}
		}

	// Add end instructions predicates for each pair procedure-line and main-line
	for ( unsigned p = 0, current_pos = 0; p <= procedures ; p++){
		for ( unsigned k = 0; k <= bound; ++k, current_pos++ ) {
			std::stringstream ss;
			if ( p < procedures ) ss << "INS-END-" << p << "-" << k; // Procedure P
			else ss << "INS-END-" << k; // Main
			endinstr.push_back( ss.str() );
			if ( 0 < k ) 
				cd->createPredicate( endinstr[current_pos] );
		}
	}

	// Add evaluation predicates
	cd->createPredicate( accu_pred );
	cd->createPredicate( eval_pred );	

	// Copy derived predicates
	//for ( unsigned i = 0; i < d->derived.size(); ++i )
	//	cd->derived.insert( new Derived( d->derived[i], *cd ));

	// Add goal predicate
	cd->createPredicate( goal_pred );
	
}


void preprocessing_jumps(){
	// detect non-static fluents
	for ( unsigned i = 0; i < d->actions.size(); ++i ) {
		IntSet s = preds( d->actions[i]->eff );
		effs.insert( s.begin(), s.end() );
	}
	// Avoid delete derived predicates in the effects
	for ( unsigned i = 0; i < d->derived.size(); ++i ){
		unclean_effs.insert( d->preds.index( d->derived[i]->name ) );
		effs.insert( d->preds.index( d->derived[i]->name ) );
	}
}

void add_jump_and_condition_predicates(){
	// Add predicates for conditions and jumps
	conds.resize( procedures + 1, StringDVec(bound) );
	jumps.resize( procedures + 1, StringDVec(bound) );
	for( unsigned p = 0; p <= procedures; p++){
		for ( unsigned k = 0; k < bound; ++k ) {
			conds[p][k].resize( d->preds.size() );
			for ( IntSet::iterator i = effs.begin(); i != effs.end(); ++i ) {
				std::stringstream ss;
				ss << "COND-" << d->preds[*i]->name << "-" << p << "-" << k; // procedure p (last one is the main)
				conds[p][k][*i] = ss.str();
				cd->createPredicate( conds[p][k][*i], d->typeList( d->preds[*i] ) );
			}
			for ( unsigned j = 0; j <= bound; ++j ) {
				std::stringstream ss;
				ss << "JMP-" << p << "-" << k << "-" << j; // procedure p (last one is the main)
				jumps[p][k].push_back( ss.str() );
				if ( j != k && j != k + 1 ) 
					cd->createPredicate( jumps[p][k][j] );
			}
		}
	}
}

void add_cost_function(bool total_cost = true){
	if(total_cost){
		// Add total-cost
		cd->createFunction( "TOTAL-COST", -1 );
	}
}

void add_program_actions(){
	unsigned tops_idx = cd->preds.index( stack_top_pred );
	IntVec tops0_v = incvec( 0, cd->preds[ tops_idx ]->params.size() );
	unsigned stp1_idx = cd->preds.index( stack_proc[ procedures ] );
	// Program and repeat actions
	for ( unsigned i = 0; i < d->actions.size(); ++i ){
		for ( unsigned k = 0; k < bound; ++k ) {
			unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );
			// Program action
			std::stringstream ss;
			ss << "PROGRAM-" << d->actions[i]->name << "-" << k;
			std::string name = ss.str();
			unsigned size = d->actions[i]->params.size();
			//unsigned size = 0;
			StringVec pars = d->typeList( d->actions[i] );
			pars.push_back( "STACKROW" );
			//Action *act = cd->createAction( name , StringVec( 1 , "STACKROW" ) );
			Action *act = cd->createAction( name , pars );
		
			cd->setPre( name, d->actions[ i ]->pre );
			act->pre->addParams( size, 1 );
			cd->addPre( 0, name, emptys[ k ] );

			cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, size ) );
			cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, size ) );
			cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, size ) );

			cd->addEff( 1, name, emptys[ k ] );
			if( INSTR_WITH_PARAMS ) cd->addEff( 0, name, instr[ i ][ procedures*bound + k ], incvec( 0, size ) );
			else cd->addEff( 0, name, instr[ i ][ procedures*bound + k ] );
			
			/*Forall * f = new Forall;
			f->params = d->actions[i]->params;
			When *w = new When();
			w->pars = d->actions[ i ]->pre;
			//w->pars->addParams( 1 , 1 );
			w->cond = d->actions[ i ]->eff;
			//w->cond->addParams( size , 1 );
			f->cond = w;
			//addStackRow( f, size );
			f->addParams( size+1 , f->params.size() );
			( ( And * )act->eff )->add( f );*/
			cd->addCost( name, 1001 );
			
			// Action to instruction
			std::vector<std::string> vinstr;
			vinstr.push_back(instr[ i ][ procedures*bound + k ]);
			act2ins[name] = vinstr;	
		}
	}
}

void add_repeat_actions(){
	unsigned tops_idx = cd->preds.index( stack_top_pred );
	IntVec tops0_v = incvec( 0, cd->preds[ tops_idx ]->params.size() );
	
	for ( unsigned i = 0; i < d->actions.size(); ++i ){
		for ( unsigned p = 0, current_pos = 0, current_ins = 0; p <= procedures ; p++, current_pos++){
			unsigned stp1_idx = cd->preds.index( stack_proc[ p ] );
			for ( unsigned k = 0; k < bound; ++k, current_pos++, current_ins++) {
				unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );
				std::stringstream ss;
				ss << "REPEAT-" << d->actions[i]->name << "-" << p << "-" << k; // Procedure P (last one is the main)
				std::string name = ss.str();

				unsigned size = d->actions[i]->params.size();
				StringVec pars = d->typeList( d->actions[i] );
				pars.push_back( "STACKROW" );
				Action *act = cd->createAction( name, pars );
		
				cd->setPre( name, d->actions[ i ]->pre );
				act->pre->addParams( size, 1 );
				if( INSTR_WITH_PARAMS ) cd->addPre( 0, name, instr[ i ][ current_ins ], incvec( 0, size ) );
				else cd->addPre( 0, name, instr[ i ][ current_ins ] );

				cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, size ) );
				cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, size ) );
				cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, size ) );

				//[BUG] Check iff the fluent is in the stack pred to add stack as param
				cd->setEff( name, d->actions[i]->eff ); 
				act->eff->addParams( size, 1 );

				cd->addEff( 1, name, cd->preds[ stl1_idx ]->name, IntVec( 1, size ) );
				cd->addEff( 0, name, cd->preds[ stl1_idx + 1 ]->name, IntVec( 1, size ) );

				addStackRow( act->pre, size );
				addStackRow( act->eff, size );

				cd->addCost( name, 1 );
			}
		}
	}
}

void add_program_jump_actions(){ // Jump actions
	unsigned tops_idx = cd->preds.index( stack_top_pred );	
	unsigned stl_idx = cd->preds.index( stack_lines[ 0 ] );
	unsigned main_idx = cd->preds.index( stack_proc[ procedures ] );
	for ( IntSet::iterator i = effs.begin(); i != effs.end(); ++i ){
		for ( unsigned j = 0; j < bound; ++j ){
			for ( unsigned k = 0; k <= bound; ++k ){
				if ( j != k && j + 1 != k ) {
					// Program jump
					std::stringstream ss;
					ss << "PROGRAM-JMP-" << cd->preds[*i]->name << "-" << j << "-" << k;

					std::string name = ss.str();
					unsigned size = d->preds[*i]->params.size();
					StringVec pars = d->typeList( d->preds[*i] );
					pars.push_back( "STACKROW" );
					cd->createAction( name, pars );

					cd->addPre( 0, name, emptys[ j ] );

					cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, size ) );
					cd->addPre( 0, name, cd->preds[ main_idx ]->name, IntVec( 1, size ) );
					cd->addPre( 0, name, cd->preds[ stl_idx + j ]->name, IntVec( 1, size ) );

					cd->addEff( 1, name, emptys[ j ] );
					cd->addEff( 0, name, conds[procedures][j][*i], incvec( 0, size ) );
					cd->addEff( 0, name, jumps[procedures][j][k] );
	
					cd->addCost( name, 1001 );

					// Action to instruction
					std::vector<std::string> vjump;
					vjump.push_back(jumps[procedures][j][k]);
					vjump.push_back(conds[procedures][j][*i]);
					act2ins[name] = vjump; 
				}
			}
		}
	}
}

void add_do_jump_actions(){
	unsigned tops_idx = cd->preds.index( stack_top_pred );
	IntVec tops0_v = incvec( 0, cd->preds[ tops_idx ]->params.size() );
	unsigned current_pos = 0;
	for ( unsigned p = 0; p <= procedures ; p++, current_pos+=(bound+1)){
		unsigned stp1_idx = cd->preds.index( stack_proc[ p ] );
		for ( unsigned k = 0; k < bound; ++k ) {
			unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );
			for ( unsigned j = 0; j <= bound; ++j ) {
				if ( j != k && j != k + 1 ) {
					unsigned stl2_idx = cd->preds.index( stack_lines[ j ] );
					// Jump when accumulator is false
					std::stringstream ss;
					ss << "DO-JMP-" << p << "-" << k << "-" << j; // Procedure P (last one is the main)
					std::string name = ss.str();
					cd->createAction( name, StringVec( 1, "STACKROW" ) );

					cd->addPre( 0, name, jumps[p][k][j] );
					cd->addPre( 0, name, eval_pred );
					cd->addPre( 1, name, accu_pred );

					cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, 0 ) );
					cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, 0 ) );
					cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 0 ) );

					cd->addEff( 1, name, eval_pred );

					cd->addEff( 1, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 0 ) );
					cd->addEff( 0, name, cd->preds[ stl2_idx ]->name, IntVec( 1, 0 ) );

					cd->addCost( name, 1 );
				}
			}
		}
	}
}

void add_do_no_jump_actions(){
	unsigned tops_idx = cd->preds.index( stack_top_pred );
	IntVec tops0_v = incvec( 0, cd->preds[ tops_idx ]->params.size() );
	unsigned current_pos = 0;
	for ( unsigned p = 0; p <= procedures ; p++, current_pos+=(bound+1)){
		unsigned stp1_idx = cd->preds.index( stack_proc[ p ] );
		for ( unsigned k = 0; k < bound; ++k ) {
			unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );
			for ( unsigned j = 0; j <= bound; ++j ){
				if ( j != k && j != k + 1 ) {
					// Skip jump when accumulator is true
					std::stringstream ss;
					ss << "DO-NOJMP-" << p << "-" << k << "-" << j; // Procedures P (last one is the main)
					std::string name = ss.str();
					cd->createAction( name, StringVec( 1, "STACKROW" ) );

					cd->addPre( 0, name, jumps[p][k][j] );
					cd->addPre( 0, name, eval_pred );
					cd->addPre( 0, name, accu_pred);

					cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, 0 ) );
					cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, 0 ) );
					cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 0 ) );

					cd->addEff( 1, name, eval_pred );
					cd->addEff( 1, name, accu_pred );

					cd->addEff( 1, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 0 ) );
					cd->addEff( 0, name, cd->preds[ stl1_idx + 1 ]->name, IntVec( 1, 0 ) );

					cd->addCost( name, 1 );
				}
			}
		}
	}
}

void add_eval_conditions(){
	unsigned tops_idx = cd->preds.index( stack_top_pred );

	unsigned current_pos = 0;
	for ( unsigned p = 0; p <= procedures ; p++, current_pos += (bound+1)){
		unsigned stp1_idx = cd->preds.index( stack_proc[ p ] );
		for ( unsigned k = 0; k < bound; ++k ) {
			unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );
			for ( IntSet::iterator i = effs.begin(); i != effs.end(); ++i ) {
				// Evaluate condition
				std::stringstream ss;
				ss << "EVAL-COND-" << d->preds[*i]->name << "-" << p << "-" << k;
				std::string name = ss.str();

				unsigned size = d->preds[*i]->params.size();
				StringVec pars = d->typeList( d->preds[*i] );
				pars.push_back( "STACKROW" );
				Action * eval = cd->createAction( name, pars );

				cd->addPre( 0, name, conds[p][k][*i], incvec( 0, size ) );

				cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, size ) );
				cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, size ) );
				cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, size ) );

				cd->addEff( 0, name, eval_pred );

				When * w = new When;
				w->cond = new And;
				( (And *) w->cond)->add( cd->ground( accu_pred ) );

				if ( unclean_effs.find( *i ) == unclean_effs.end() ) {
					/*if( stack_pred.find( d->preds[ *i ]->name ) != stack_pred.end() )
						w->pars = cd->ground( d->preds[*i]->name, incvec( 0, size+1 ) );
					else
						w->pars = cd->ground( d->preds[*i]->name, incvec( 0, size ) );
					( ( And * )eval->eff )->add( w );*/
					w->pars = cd->ground( d->preds[*i]->name, incvec( 0, size ) );
					addStackRow( w , size );
					( ( And * )eval->eff )->add( w );
				}
				else{
					for ( unsigned j = 0; j < d->derived.size(); ++j ){
						if ( d->derived[j]->name == d->preds[*i]->name ) {
							// To allow jumps over derived predicates
							( ( And * )eval->eff )->add( new Not( cd->ground( d->derived[j]->name, incvec( 0, d->derived[j]->params.size() ) ) ) );
							( (And *) w->cond)->add( cd->ground( d->derived[j]->name, incvec( 0, d->derived[j]->params.size() ) ) );
							Exists * exists = dynamic_cast< Exists * >( d->derived[j]->cond );
							//Forall * forall = dynamic_cast< Forall * >( d->derived[j]->cond );
							if ( exists ) {
								//std::cout << "Here?\n";
								w->pars = exists->cond->copy( *cd );
								w->addParams( d->derived[j]->params.size(), 1 );
								Forall * f = new Forall;
								f->params = exists->params;
								f->cond = w;
								addStackRow( f, size );
								( ( And * )eval->eff )->add( f );
							}
							else {
								w->pars = d->derived[j]->cond->copy( *cd );
								w->addParams( d->derived[j]->params.size(), 1 );
								addStackRow( w, size );
								( ( And * )eval->eff )->add( w );
							}
							break;
						}
					}
				}

				cd->addCost( name, 1 );
			}
		}
	}
}

void add_program_end(){
	unsigned main_idx = cd->preds.index( stack_proc[ procedures ] );
	unsigned tops_idx = cd->preds.index( stack_top_pred );
	IntVec tops0_v = incvec( 0, cd->preds[ tops_idx ]->params.size());
	unsigned current_pos = procedures*(bound+1);
	//Program end action for main, first test
	for ( unsigned k = 1; k <= bound; ++k ) {
		unsigned stl_idx = cd->preds.index( stack_lines[ k ] );
		std::stringstream ss;
		ss << "PROGRAM-END-" << k;
		std::string name = ss.str();
		//Action * endact = 
		cd->createAction( name, StringVec( 1, "STACKROW" ) );

		cd->addPre( 0, name, emptys[ k ] );
		cd->addPre( 0, name, tests[ 0 ] );

		cd->addPre( 0, name, cd->preds[ tops_idx ]->name, IntVec( 1, 0 ) );
		cd->addPre( 0, name, cd->preds[ main_idx ]->name, IntVec( 1, 0 ) );
		cd->addPre( 0, name, cd->preds[ stl_idx ]->name, IntVec( 1, 0 ) );

		cd->addEff( 1, name, emptys[ k ] );
		cd->addEff( 0, name, endinstr[ current_pos + k ] );
			
		// Action to instruction
		std::vector< std::string > vendinstr;
		vendinstr.push_back( endinstr[ current_pos + k ] );
		act2ins[name] = vendinstr;

		cd->addCost( name, 1 );
	}
}

void add_repeat_end_actions(){
	unsigned main_idx = cd->preds.index( stack_proc[ procedures ] );
	unsigned nsr_idx = cd->preds.index( stack_row_pred );
	unsigned top_idx = cd->preds.index( stack_top_pred );
	int ci = cd->constantIndex( rows[ 0 ] , "STACKROW" );

	// Action for the Main in the row0 of the stack
	for( unsigned t = 0; t < total_instances; t++){
		for( unsigned k = 1; k <= bound; k++ ){
			unsigned stl_idx = cd->preds.index( stack_lines[ k ] );
			std::stringstream ss;
			ss << "REPEAT-END-MAIN-" << k << "-" << t;
			std::string name = ss.str();

			Action * endact = cd->createAction( name, StringVec( 1, "STACKROW" ) );

			cd->addPre( 0, name, endinstr[ procedures * (bound+1) + k] );
			cd->addPre( 0, name, tests[ t ] );			
			//cd->addPre( 0, name, cd->preds[ top_idx ]->name,  IntVec( 1, 0 ) );
			cd->addPre( 0, name, cd->preds[ main_idx ]->name, IntVec( 1, 0 ) );
			cd->addPre( 0, name, cd->preds[ stl_idx ]->name, IntVec( 1, 0 ) );
			cd->addPre( 0, name, stack_top_pred, IntVec( 1 , ci ) );

			for ( unsigned i = 0; i < ins[ t ]->goal.size(); ++i ) {
				IntVec v;
				StringVec tnc = d->objectList( ins[ t ]->goal[i] );
				Lifted * l = d->preds.get( ins[ t ]->goal[ i ]->name );
				for ( unsigned j = 0; j < tnc.size(); ++j )
					v.push_back( cd->constantIndex( tnc[ j ], d->types[ l->params[ j ] ]->name ) );
				cd->addPre( 0, name, ins[ t ]->goal[ i ]->name, v );
			}

			if( t + 1 == total_instances ){
				cd->addEff( 0, name, goal_pred );
			}
			else{
				cd->addEff( 1, name, tests[ t ] );
				cd->addEff( 0, name, tests[ t + 1 ] );
				cd->addEff( 1, name, cd->preds[ stl_idx ]->name, IntVec( 1 , 0 ) );
				cd->addEff( 0, name, cd->preds[ stl_idx - k ]->name, IntVec( 1 , 0 ) );
				
				std::vector< std::pair< std::string , IntVec > > add_eff;
				
				// Add effects of the next instance
				for ( unsigned i = 0; i < ins[ t + 1 ]->init.size(); ++i ){ 
					IntVec v;
					StringVec tnc = d->objectList(ins[t + 1 ]->init[ i ]); 
					Lifted *l = d->preds.get(ins[ t + 1 ]->init[ i ]->name ); 
					for( unsigned j = 0; j < tnc.size(); ++j){ 
						v.push_back( cd->constantIndex( tnc[ j ], d->types[ l->params[ j ] ]->name ) );
					}
					cd->addEff( 0, name, ins[ t + 1 ]->init[ i ]->name, v);	
					add_eff.push_back( make_pair( ins[ t + 1 ]->init[ i ]->name , v ) );
				}

				// Delete fluents of the current instance
				for ( unsigned i = 0; i < d->preds.size(); ++i ) {
					if( unclean_effs.find(i) != unclean_effs.end() ) continue;
					//if( effs.find(i) == effs.end() ) continue; //[PERFORMANCE] static fluents
					if( cd->preds[ i ]->params.size() ){
						Forall * f = new Forall;
						f->params = cd->preds[ i ]->params;
						//f->cond = new Not( cd->ground( d->preds[ i ]->name, incvec( 1, f->params.size() + 1 ) ) );
						When *w = new When();
						w->pars = new And();
						( ( And * ) w->pars )->add( cd->ground( d->preds[ i ]->name, incvec( 1, f->params.size() + 1 ) ) );
						for( size_t j = 0; j < add_eff.size(); j++ ){
							if( add_eff[ j ].first == d->preds[ i ]->name ){
								( ( And * ) w->pars )->add( new Not( cd->ground( d->preds[ i ]->name, add_eff[ j ].second ) ) );
							}
						}
						//w->cond = d->actions[ i ]->eff;
						w->cond = new Not( cd->ground( d->preds[ i ]->name, incvec( 1, f->params.size() + 1 ) ) );
						f->cond = w;
						( ( And * )endact->eff )->add( f );
					}
					else
						cd->addEff( 1, name, d->preds[i]->name );
				}
				
			}

			cd->addCost( name, 1 );
		}
	}

	// Repeat ends for other procedures or the main recursion
	for( unsigned p = 0; p <= procedures ; p++ ){
		unsigned stp1_idx = cd->preds.index( stack_proc[ p ] );
		for( unsigned t = 0; t < total_instances; t++){
			for ( unsigned k = 1; k <= bound; ++k ) {
				unsigned stl1_idx = cd->preds.index( stack_lines[ k ] );

				std::stringstream ss;
				ss << "REPEAT-END-" << p << "-" << k << "-" << t;
				std::string name = ss.str();

				Action * endact = cd->createAction( name, StringVec( 2, "STACKROW" ) );

				cd->addPre( 0, name, endinstr[ p * (bound+1) + k] );
				cd->addPre( 0, name, tests[t] );
				cd->addPre( 0, name, cd->preds[ nsr_idx ]->name , incvec( 0, 2 ) );					
				cd->addPre( 0, name, cd->preds[ top_idx ]->name , IntVec( 1, 1 ) );
				cd->addPre( 0, name, cd->preds[ stp1_idx ]->name, IntVec( 1, 1 ) );
				cd->addPre( 0, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 1 ) );

				cd->addEff( 1, name, cd->preds[ top_idx ]->name , IntVec( 1, 1 ) );
				cd->addEff( 0, name, cd->preds[ top_idx ]->name , IntVec( 1, 0 ) );
				cd->addEff( 1, name, cd->preds[ stp1_idx ]->name, IntVec( 1, 1 ) );
				cd->addEff( 1, name, cd->preds[ stl1_idx ]->name, IntVec( 1, 1 ) );
				
				//[BUG] Add Forall delete ASSIGNMENT from this stackrow
				for( std::set< std::string >::iterator it = stack_pred.begin(); it != stack_pred.end(); it++ ){
					Forall * f = new Forall;
					Lifted *l = d->preds.get( *it );
					f->params = l->params;
					IntVec v;
					for(size_t i = 0; i < f->params.size(); i++)
						v.push_back( i+2 );
					v.push_back( 1 );
					f->cond = new And;	
					( ( And * )f->cond )->add( new Not( cd->ground( *it, v ) ) );
					( ( And * )endact->eff )->add( f );
				}

				cd->addCost( name, 1 );
			}
		}
	}

}

void add_program_call_actions(){
	//unsigned nsr_idx = cd->preds.index( stack_row_pred );
	unsigned top_idx = cd->preds.index( stack_top_pred );
	unsigned main_idx = cd->preds.index( stack_proc[ procedures ] );
	
	for(unsigned p = 0; p <= procedures; p++ ){

		for( unsigned l = 0; l < bound; l++ ){
			unsigned stl_idx = cd->preds.index( stack_lines[ l ] );
			std::stringstream ss;
			ss << "PROGRAM-CALL-" << p << "-" << procedures << "-" << l;

			std::string name = ss.str();
	
			StringVec sv( 1 , "STACKROW" );
				
			for( std::set< std::string >::iterator it = stack_vars.begin() ; it != stack_vars.end() ; it++){
				sv.push_back( *it );
			}

			cd->createAction( name, sv );

			cd->addPre( 0, name, emptys[ l ] );
			cd->addPre( 0, name, cd->preds[ top_idx ]->name , IntVec( 1, 0 ) );
			cd->addPre( 0, name, cd->preds[ main_idx ]->name, IntVec( 1, 0 ) );
			cd->addPre( 0, name, cd->preds[ stl_idx ]->name , IntVec( 1, 0 ) );

			cd->addEff( 1, name, emptys[ l ] );
			// include this fluents to allow repetitions
			cd->addEff( 0, name, calls[ p ][ procedures * bound + l ] , incvec( 1 , sv.size() ) ); 

			cd->addCost( name, 1001);

			// Action to instruction
			std::vector< std::string > vcalls;
			vcalls.push_back( calls[ p ][ procedures * bound + l ] );
			act2ins[ name ] = vcalls;
		}
	}
}
	
void add_repeat_call_actions(){
	
	// CALL P from P2
	for(unsigned p = 0; p <= procedures; p++ ){
		for( unsigned p2 = p; p2 <= procedures; p2++){
			for( unsigned l = 0; l < bound; l++ ){
				std::stringstream ss;
				ss << "REPEAT-CALL-" << p << "-" << p2 << "-" << l;
				std::string name = ss.str();

				StringVec sv( 2 , "STACKROW" );
				
				for( std::set< std::string >::iterator it = stack_vars.begin() ; it != stack_vars.end() ; it++){
					sv.push_back( *it );
				}

				Action *repcall = cd->createAction( name, sv );
				
				cd->addPre( 0, name, calls[ p ][ p2 * bound + l ] , incvec( 2 , sv.size() ) );

				cd->addPre( 0, name, stack_row_pred  , incvec( 0, 2 ) );
				cd->addPre( 0, name, stack_top_pred  , IntVec( 1, 0 ) );
				cd->addPre( 0, name, stack_proc[ p2 ], IntVec( 1, 0 ) );
				cd->addPre( 0, name, stack_lines[ l ] , IntVec( 1, 0 ) );

				cd->addEff( 1, name, stack_top_pred  , IntVec( 1, 0 ) );
				cd->addEff( 0, name, stack_top_pred  , IntVec( 1, 1 ) );

				cd->addEff( 0, name, stack_proc[ p ]	 , IntVec( 1, 1 ) );
				cd->addEff( 0, name, stack_lines[ 0 ]	 , IntVec( 1, 1 ) );
				cd->addEff( 1, name, stack_lines[ l ]	 , IntVec( 1, 0 ) );
				cd->addEff( 0, name, stack_lines[ l + 1 ], IntVec( 1, 0 ) );

				//[BUG][ToDo] This few lines have big chances of being future bugs
				int predi = 0;
				for( std::set< std::string >::iterator it = stack_pred.begin(); it != stack_pred.end(); it++, predi++){
					Lifted *l = cd->preds.get( *it );
					if( l->params.size() > 1 ){
						Forall *f = new Forall;
						When *w = new When;
						IntVec wp1, wp2;
						for(size_t par = 2; par < sv.size(); par++){
							wp1.push_back( par );
							Type *t = cd->getType( sv[ par ] );
							std::pair< bool, int > pbi = t->parseConstant( stack_cons[ predi ][ par - 2 ] );
							wp2.push_back( pbi.second );
							//wp2.push_back( -2 );
						}
						//Add the correct params to Forall
						for(size_t par = 0; par < (l->params).size(); par++){
							bool ok = true;
							for(size_t par2 = 0; par2 < (repcall->params).size(); par2++){
								if( (l->params)[par] == (repcall->params)[par2] ){
									ok = false; break;
								}
							}
							if( ok ){
								(f->params).push_back( (l->params)[par] );
				
							}
						}
						for( size_t par = 0; par < (f->params).size(); par++){
							wp1.push_back( par + (repcall->params).size());
							wp2.push_back( par + (repcall->params).size());
						}
						wp1.push_back( 0 );
						wp2.push_back( 1 );
						w->pars = cd->ground( l->name, wp1 );
						w->cond = cd->ground( l->name, wp2 );
						f->cond = w;
						( ( And * ) repcall->eff )->add( f );
					}
					else{
						When *w = new When;
						( ( And * ) repcall->eff )->add( w );
					}
				}

				cd->addCost( name, 1);
			}
		}
	}
}

void create_instance(){
	cins = new Instance( *cd );
	cins->name = ins[0]->name;
	cins->metric = true;

	// copy initial state
	for ( unsigned i = 0; i < ins[0]->init.size(); ++i ){
		Ground *g = ins[ 0 ]->init[ i ];
		Type *t = d->getType( g->name );
		int predi = d->preds.index( t->name );
		if ( predi  >= 0 ){			
			StringVec params = d->objectList( g );
			// Add row0 for local predicates
			if( stack_pred.find( t->name ) != stack_pred.end() ){
				stack_cons.push_back( StringVec() );
				for( unsigned j = 0; j<  params.size(); ++j)
					// Add the name of the base constants for local predicates
					if( stack_vars.find( d->types[ g->lifted->params[ j ] ]->name ) != stack_vars.end() ){
						stack_cons[ stack_cons.size() - 1 ].push_back( params[ j ] );
					}
				params.push_back( rows[ 0 ] );
			}
			cins->addInit( t->name, params );
		}
	}

	// add initial test
	cins->addInit( tests[0] );

	// add stack rows connections
	for( unsigned i = 1; i < stack_size; i++){
		StringVec current_rows;
		current_rows.push_back( rows[i-1] );
		current_rows.push_back( rows[i] );
		cins->addInit( stack_row_pred , current_rows );
	}

	// start programming with main
	StringVec params;
	params.push_back( rows[0] );
	cins->addInit( stack_proc[procedures], params );

	// start of first line
	cins->addInit( stack_lines[0], params );

	// set the top row of the stack
	cins->addInit( stack_top_pred, params );

	// instructions are empty
	for ( unsigned k = 0; k <= bound; ++k ) {
		cins->addInit( emptys[ k ] );
	}

	// we are done programming (only for mains)
	cins->addGoal( goal_pred );
	
}

void print_domain(){
	cd->PDDLPrint( std::cout );
}

void print_instance(){
	cins->PDDLPrint( std::cerr );
}

void print_act2ins(){
	// Print actions to instructions file
	std::ofstream ofs("act2ins.txt");
	for(std::map<std::string,std::vector<std::string> >::iterator it = act2ins.begin(); it != act2ins.end(); it++){
		std::vector<std::string > vs = it->second;
		ofs<<it->first;
		for(int i=0;i < int(vs.size()); i++){
			ofs<<" "<<vs[i];
		}
		ofs<<std::endl;
	}
	ofs.close();
}

void clean_variables(){
	delete cins;
	delete cd;
	for(unsigned i = 0; i < total_instances; i++)
		delete ins[i];
	delete d;
	
}

/****** COMPILER MAIN ***********/
int main( int argc, char *argv[] ) {

	init(argc, argv); //std::cout << "INIT" << std::endl;
	create_compiled_domain( d->name ); //std::cout << "CD" << std::endl;
	add_types(); //std::cout << "T" << std::endl;
	add_predicates(); //std::cout << "P" << std::endl;
	create_instance(); //std::cout << "CI" << std::endl;
	preprocessing_jumps(); //std::cout << "PJ" << std::endl;
	add_jump_and_condition_predicates(); //std::cout << "JCP" << std::endl;
	add_cost_function(); //std::cout << "CF" << std::endl;
	if( to_program )
		add_program_actions(); //std::cout << "PA" << std::endl;
	add_repeat_actions(); //std::cout << "RA" << std::endl;
	if( to_program )
		add_program_jump_actions(); //std::cout << "PJA" << std::endl;
	add_do_jump_actions(); //std::cout << "DJA" << std::endl;
	add_eval_conditions(); //std::cout << "EC" << std::endl;
	add_do_no_jump_actions(); //std::cout << "DNJA" << std::endl;
	if( to_program )
		add_program_end(); //std::cout << "PE" << std::endl;
	add_repeat_end_actions(); //std::cout << "REA" << std::endl;	
	if( to_program )
		add_program_call_actions(); //std::cout << "PCA" << std::endl;
	add_repeat_call_actions(); //std::cout << "RCA" << std::endl;
	print_domain(); //std::cout << "PD" << std::endl;
	print_instance(); //std::cout << "PI" << std::endl;
	print_act2ins(); //std::cout << "PA2I" << std::endl;
	clean_variables(); //std::cout << "CV" << std::endl;
	
}
