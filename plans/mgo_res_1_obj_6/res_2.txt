1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2083
parse actions...
Number of actions: 10488
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1201
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1356 variables of 2083 necessary
0 of 0 mutex groups necessary.
10227 of 10488 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 2712
Preprocessor derived variables: 0
Preprocessor task size: 112080
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.36s]
building causal graph...done! [t=0.40s]
packing state variables...done! [t=0.40s]
Variables: 1356
Facts: 2712
Bytes per state: 172
done initalizing global data [t=0.40s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 50399 unary operators... done! [36694 unary operators]
Best heuristic value: 2 [g=0, 1 evaluated, 0 expanded, t=0.46s, 23512 KB]
Best heuristic value: 1 [g=1, 2 evaluated, 1 expanded, t=0.46s, 23512 KB]
Solution found!
Actual search time: 0.06s [t=0.46s]
tr-c56-N-c37 (1)
dummy-action-37 (1)
Plan length: 2 step(s).
Plan cost: 2
Initial state h value: 2.
Expanded 2 state(s).
Reopened 0 state(s).
Evaluated 3 state(s).
Evaluations: 3
Generated 15 state(s).
Dead ends: 0 state(s).
Search time: 0.06s
Total time: 0.46s
Solution found.
Peak memory: 23512 KB

Plans and cost: 

