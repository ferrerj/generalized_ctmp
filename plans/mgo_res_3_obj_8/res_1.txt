1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 10655
parse actions...
Number of actions: 87318
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 6587
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
5924 variables of 10655 necessary
0 of 0 mutex groups necessary.
86315 of 87318 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 11848
Preprocessor derived variables: 0
Preprocessor task size: 1975324
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=7.86s]
building causal graph...done! [t=9.52s]
packing state variables...done! [t=9.52s]
Variables: 5924
Facts: 11848
Bytes per state: 744
done initalizing global data [t=9.52s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 920233 unary operators... done! [702367 unary operators]
Best heuristic value: 3 [g=0, 1 evaluated, 0 expanded, t=10.24s, 416840 KB]
Best heuristic value: 2 [g=1, 2 evaluated, 1 expanded, t=10.28s, 416840 KB]
Best heuristic value: 1 [g=2, 3 evaluated, 2 expanded, t=10.32s, 416840 KB]
Solution found!
Actual search time: 0.80s [t=10.32s]
tr-c303-NW-c289 (1)
tr-c289-NE-c277 (1)
dummy-action-277 (1)
Plan length: 3 step(s).
Plan cost: 3
Initial state h value: 3.
Expanded 3 state(s).
Reopened 0 state(s).
Evaluated 4 state(s).
Evaluations: 4
Generated 22 state(s).
Dead ends: 0 state(s).
Search time: 0.80s
Total time: 10.32s
Solution found.
Peak memory: 416840 KB

Plans and cost: 

