rm plan.txt
touch plan.txt
bound=`cat plan.out | wc -l`
count=1
until [ $count -gt $bound ]
do
	./initiator.sh plan.txt $count
	mv ins2.pddl ins.pddl
	./planner.sh domain.pddl ins.pddl > res_$count.txt
	cp sas_plan plan_$count.txt
	cp sas_plan plan.txt
	((count++))
done
cp inst_domain.pddl ins.pddl
rm output*
rm plan.txt
