1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6827
parse actions...
Number of actions: 156780
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5300
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4088 variables of 6827 necessary
0 of 0 mutex groups necessary.
156766 of 156780 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 8176
Preprocessor derived variables: 0
Preprocessor task size: 5157230
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=19.16s]
building causal graph...done! [t=23.92s]
packing state variables...done! [t=23.92s]
Variables: 4088
Facts: 8176
Bytes per state: 512
done initalizing global data [t=23.92s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 2335207 unary operators... done! [1290873 unary operators]
Best heuristic value: 11 [g=0, 1 evaluated, 0 expanded, t=25.64s, 1060828 KB]
Best heuristic value: 10 [g=1, 3 evaluated, 2 expanded, t=25.96s, 1060828 KB]
Best heuristic value: 9 [g=2, 4 evaluated, 3 expanded, t=26.12s, 1060828 KB]
Best heuristic value: 8 [g=3, 5 evaluated, 4 expanded, t=26.28s, 1060828 KB]
Best heuristic value: 7 [g=4, 6 evaluated, 5 expanded, t=26.44s, 1060828 KB]
Best heuristic value: 6 [g=5, 7 evaluated, 6 expanded, t=26.60s, 1060828 KB]
Best heuristic value: 5 [g=6, 8 evaluated, 7 expanded, t=26.74s, 1060828 KB]
Best heuristic value: 4 [g=7, 9 evaluated, 8 expanded, t=26.90s, 1060828 KB]
Best heuristic value: 3 [g=8, 10 evaluated, 9 expanded, t=27.04s, 1060828 KB]
Best heuristic value: 2 [g=9, 11 evaluated, 10 expanded, t=27.18s, 1060828 KB]
Best heuristic value: 1 [g=10, 12 evaluated, 11 expanded, t=27.34s, 1060828 KB]
Solution found!
Actual search time: 3.42s [t=27.34s]
p-o0 (1)
tr-c386-NW-c320 (1)
tr-c320-NW-c254 (1)
tr-c254-NW-c188 (1)
tr-c188-SW-c248 (1)
tr-c248-NW-c182 (1)
rr-c182-CCW-c639 (1)
tr-c639-W-c635 (1)
rr-c635-CCW-c177 (1)
g-c177-o1-c3 (1)
dummy-grasping-o1-action (1)
Plan length: 11 step(s).
Plan cost: 11
Initial state h value: 11.
Expanded 12 state(s).
Reopened 0 state(s).
Evaluated 13 state(s).
Evaluations: 13
Generated 115 state(s).
Dead ends: 0 state(s).
Search time: 3.42s
Total time: 27.34s
Solution found.
Peak memory: 1060828 KB

Plans and cost: 

