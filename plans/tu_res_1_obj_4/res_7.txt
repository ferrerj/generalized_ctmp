1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1319
parse actions...
Number of actions: 14108
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 913
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
809 variables of 1319 necessary
0 of 0 mutex groups necessary.
14099 of 14108 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1618
Preprocessor derived variables: 0
Preprocessor task size: 168962
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.64s]
building causal graph...done! [t=0.80s]
packing state variables...done! [t=0.82s]
Variables: 809
Facts: 1618
Bytes per state: 104
done initalizing global data [t=0.82s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 81303 unary operators... done! [55974 unary operators]
Best heuristic value: 9 [g=0, 1 evaluated, 0 expanded, t=1.06s, 31444 KB]
Best heuristic value: 7 [g=1, 4 evaluated, 3 expanded, t=1.06s, 31444 KB]
Best heuristic value: 6 [g=2, 5 evaluated, 4 expanded, t=1.06s, 31444 KB]
Best heuristic value: 5 [g=3, 6 evaluated, 5 expanded, t=1.06s, 31444 KB]
Best heuristic value: 4 [g=4, 7 evaluated, 6 expanded, t=1.06s, 31444 KB]
Best heuristic value: 3 [g=5, 8 evaluated, 7 expanded, t=1.06s, 31444 KB]
Best heuristic value: 2 [g=6, 9 evaluated, 8 expanded, t=1.06s, 31444 KB]
Best heuristic value: 1 [g=7, 10 evaluated, 9 expanded, t=1.06s, 31444 KB]
Solution found!
Actual search time: 0.24s [t=1.06s]
p-o2 (1)
tr-c60-W-c57 (1)
rr-c57-CCW-c141 (1)
tr-c141-SW-c163 (1)
tr-c163-NW-c133 (1)
tr-c133-NW-c103 (1)
g-c103-o3-c7 (1)
dummy-grasping-o3-action (1)
Plan length: 8 step(s).
Plan cost: 8
Initial state h value: 9.
Expanded 10 state(s).
Reopened 0 state(s).
Evaluated 11 state(s).
Evaluations: 11
Generated 110 state(s).
Dead ends: 0 state(s).
Search time: 0.24s
Total time: 1.06s
Solution found.
Peak memory: 31444 KB

Plans and cost: 

