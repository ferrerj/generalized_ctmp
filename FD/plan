#! /bin/bash

# set -e
# ulimit -m 4000000
# #Time limit 4h
# ulimit -t 14500

BASEDIR="$(dirname "$0")"

function die {
    echo "$@" 1>&2
    exit 1
}

function usage {
    die "usage: $(basename "$0") [DOMAIN_FILE] PROBLEM_FILE SEARCH_OPTION ..."
}

# Paths to planner components
#Modified by A.Albore to run in the Orion cluster

TRANSLATE="$BASEDIR/simple-converter"
PREPROCESS="$BASEDIR/preprocess"
SEARCH="$BASEDIR/downward-release"

# Need to explicitly ask for GNU time (from MacPorts) on Mac OS X.
if [[ "$(uname)" == "Darwin" ]]; then
    TIME="gtime"
    if ! which $TIME >/dev/null; then
        die "$TIME must be installed on Mac OSX (from MacPorts, perhaps) for this to work"
    fi
else
    TIME="command time"
fi

TIME="$TIME --output=elapsed.time --format=%S\n%U\n"

if [[ "$#" < 2 ]]; then
    usage
fi

echo "1. Running translator"
if [[ -e "$2" ]]; then
    echo "Second argument is a file name: use two translator arguments."
    $TIME "$TRANSLATE" "$1" "$2"
    shift 2
else
    echo "Second argument is not a file name: auto-detect domain file."
    $TIME "$TRANSLATE" "$1"
    shift
fi
echo

echo "2. Running preprocessor"
$TIME --append "$PREPROCESS" < output.sas
echo

echo "3. Running search"
case "$1" in
'hadd')
	shift
	"$SEARCH" --heuristic "hadd=add()" --search "eager(single(hadd), preferred=hadd, reopen_closed=false,pathmax=false)" < output "$@"
	;;
'hcg')
	shift
	"$SEARCH" --heuristic "hcg=cg()" --search "eager(single(hcg), preferred=hcg, reopen_closed=false,pathmax=false)" < output "$@"
	;;
'lmcut')
	shift
	"$SEARCH" --search "astar(lmcut())" < output "$@"
	;;

'robotics')
    shift
         "$SEARCH" --heuristic "hlm,hff=lm_ff_syn(lm_rhw(reasonable_orders=true))" --search "iterated([
            lazy_greedy([hff,hlm],preferred=[hff,hlm])],repeat_last=true,continue_on_fail=true)" --always < output "$@"
        ;;

'lama') 
	shift
        "$SEARCH" --heuristic "hlm=lmcount(lm_rhw(reasonable_orders=true,lm_cost_type=2,cost_type=2),pref=true)" --search "lazy(single(hlm))" < output "$@"
	;;
'alex')
	shift
        "$SEARCH" --heuristic "hcg=cg()" --heuristic "hlm=lmcount(lm_rhw(reasonable_orders=true,lm_cost_type=2,cost_type=2),pref=true)" --search "lazy_greedy([hcg,hlm],preferred=[hcg,hlm],boost=1000)" < output "$@"
	;;
*) 
	"$SEARCH" < output "$@"
	;;
esac
echo
echo "Plans and cost: "
cat plan_numbers_and_cost
echo
