#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<map>
#include<set>


using namespace std;


set<string> consistent;
map<int, pair<string, vector<string> > > plan;
vector<string> accumulated_goals;


//Translate action to a dummy goal for the task planner
string translate( const string &action, const vector<string> &params ) {
	
	if(action == "grasp" ) 
		return "(dummy_grasping_" + params[ 0 ]+")";
	else if(action == "place" ) 
		return "(" + params[ 1 ] + ")";

}

//Goals which should be accumulated
void set_consistent() {
	consistent.insert("(dummy_goal_o0)");
	consistent.insert("(dummy_goal_o1)");
	consistent.insert("(dummy_goal_o2)");

}


//Check if a goal is consistent or not. If it is, must be accumulated
bool isConsistent(string dummy_goal) {
	const bool is_in = consistent.find( dummy_goal ) != consistent.end();
	accumulated_goals.push_back( dummy_goal );
	return is_in;
}

//Print
void print() {

	map<int, pair<string, vector<string> > >::iterator it;
	for( it = plan.begin(); it != plan.end(); ++it ) {
		string dummy = translate( it->second.first, it->second.second );
		//cout << dummy  << endl;
		//cout << "Is consistent?: " << isConsistent(dummy) << endl;
		cout << dummy << " " << isConsistent( dummy ) << " " << it->second.first << endl;
	}
}



int main() {

	set_consistent();

	string line, word;
	int k = 0;
	while( getline( cin , line ) ) {
		stringstream iss( line );
		string act;
		vector< string > params;
		iss >> act;
		size_t from = act.find_first_of( "-" ) + 1;
		size_t last = act.find_last_of( "-" ) - 1;
		last = act.find_last_of( "-" , last ) ;
		size_t tam = last - from ;
		act = act.substr( from, tam );
		//cerr << "ACTION: " ;
		//cerr << act << endl;
		while( iss >> word ){
			params.push_back( word );
		}
		//cerr << "PARAMS:";
		params.resize( params.size() - 1 );
		
		/*for( int i = 0; i < int( params.size() );i++ ){
			cerr << " " << params[ i ];
		} 
		cerr << endl;*/

		//filter next ball and end repeats
		if( act == "grasp" || act == "place" ){
			plan.insert( make_pair( k, make_pair( act , params ) ) );
			k++;
		}	
	}

	print();
}
