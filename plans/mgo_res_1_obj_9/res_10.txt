1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 2899
parse actions...
Number of actions: 15339
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 1648
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
1943 variables of 2899 necessary
0 of 0 mutex groups necessary.
15078 of 15339 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 3886
Preprocessor derived variables: 0
Preprocessor task size: 166017
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.56s]
building causal graph...done! [t=0.64s]
packing state variables...done! [t=0.64s]
Variables: 1943
Facts: 3886
Bytes per state: 244
done initalizing global data [t=0.64s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 75077 unary operators... done! [54595 unary operators]
Best heuristic value: 13 [g=0, 1 evaluated, 0 expanded, t=0.76s, 32140 KB]
Best heuristic value: 12 [g=1, 2 evaluated, 1 expanded, t=0.76s, 32140 KB]
Best heuristic value: 11 [g=2, 3 evaluated, 2 expanded, t=0.76s, 32140 KB]
Best heuristic value: 10 [g=3, 4 evaluated, 3 expanded, t=0.76s, 32140 KB]
Best heuristic value: 9 [g=4, 5 evaluated, 4 expanded, t=0.76s, 32140 KB]
Best heuristic value: 8 [g=5, 6 evaluated, 5 expanded, t=0.76s, 32140 KB]
Best heuristic value: 7 [g=7, 8 evaluated, 7 expanded, t=0.76s, 32140 KB]
Best heuristic value: 6 [g=9, 11 evaluated, 10 expanded, t=0.76s, 32140 KB]
Best heuristic value: 5 [g=11, 14 evaluated, 13 expanded, t=0.76s, 32140 KB]
Best heuristic value: 4 [g=12, 15 evaluated, 14 expanded, t=0.76s, 32140 KB]
Best heuristic value: 3 [g=13, 16 evaluated, 15 expanded, t=0.76s, 32140 KB]
Best heuristic value: 2 [g=14, 17 evaluated, 16 expanded, t=0.76s, 32140 KB]
Best heuristic value: 1 [g=15, 18 evaluated, 17 expanded, t=0.76s, 32140 KB]
Solution found!
Actual search time: 0.12s [t=0.76s]
tr-c99-SW-c109 (1)
rr-c109-CCW-c50 (1)
tr-c50-W-c48 (1)
tr-c48-W-c46 (1)
tr-c46-NW-c25 (1)
g-c25-o8-c180 (1)
to-c25-c27-E-o8-c180-c30 (1)
to-c27-c29-E-o8-c30-c33 (1)
ro-c29-c96-CCW-o8-c33-c16 (1)
to-c96-c98-E-o8-c16-c17 (1)
ro-c98-c30-CCW-o8-c17-c10 (1)
p-o8 (1)
tr-c30-E-c33 (1)
g-c33-o3-c11 (1)
to-c33-c17-N-o3-c11-c155 (1)
dummy-action-17 (1)
Plan length: 16 step(s).
Plan cost: 16
Initial state h value: 13.
Expanded 18 state(s).
Reopened 0 state(s).
Evaluated 19 state(s).
Evaluations: 19
Generated 74 state(s).
Dead ends: 0 state(s).
Search time: 0.12s
Total time: 0.76s
Solution found.
Peak memory: 32140 KB

Plans and cost: 

