1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 8551
parse actions...
Number of actions: 66310
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5341
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4662 variables of 8551 necessary
0 of 0 mutex groups necessary.
65280 of 66310 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 9324
Preprocessor derived variables: 0
Preprocessor task size: 1489433
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=5.92s]
building causal graph...done! [t=7.16s]
packing state variables...done! [t=7.16s]
Variables: 4662
Facts: 9324
Bytes per state: 584
done initalizing global data [t=7.16s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 691278 unary operators... done! [527844 unary operators]
Best heuristic value: 4 [g=0, 1 evaluated, 0 expanded, t=7.70s, 340160 KB]
Best heuristic value: 3 [g=1, 2 evaluated, 1 expanded, t=7.72s, 340160 KB]
Best heuristic value: 2 [g=2, 3 evaluated, 2 expanded, t=7.74s, 340160 KB]
Best heuristic value: 1 [g=3, 4 evaluated, 3 expanded, t=7.78s, 340160 KB]
Solution found!
Actual search time: 0.62s [t=7.78s]
tr-c196-E-c199 (1)
tr-c199-E-c201 (1)
tr-c201-E-c203 (1)
dummy-action-203 (1)
Plan length: 4 step(s).
Plan cost: 4
Initial state h value: 4.
Expanded 4 state(s).
Reopened 0 state(s).
Evaluated 5 state(s).
Evaluations: 5
Generated 9 state(s).
Dead ends: 0 state(s).
Search time: 0.62s
Total time: 7.78s
Solution found.
Peak memory: 340160 KB

Plans and cost: 

