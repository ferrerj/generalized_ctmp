\section{Modeling Task and Motion Planning}

Planning in robotics involve a robot that can act in an environment while avoiding collisions and objects which can be moved by the robot. The robot and objects have different shapes and sizes so geometrical representation is essential for these problems. Consider a general type of problem where a robot, which we represent as a stick shape robot with a gripper, that can translate and rotate from one configuration to another one, grasp objects and place them in a 2D world. Configurations for the robot $conf(r)$ and the objects $conf(o)$  are captured by triplets $<x, y, \theta>$ where $x$ and $y$ represent the center of the mass of a body (robot or object) and $\theta$ the orientation based on the world coordinates. The configuration space is discretized  according a resolution parameter $r$ for a grid world of size $r \times r$ where $r \in {10, 30, \dots}$ and orientation angles are discretized into $r_a = 8$ angles where $360\degree/r_a = \# degrees$ that the robot can rotate. Thus, the possible number of configurations for each robot and object is in the order of $\#c = r_a \times r^2$, and since the gripper can be in $n+1$ states, where $n$ is the number of objects that can be grasped, then the total size $\abs{S}$ of the state space is $(n+1) \times \#c^{n+1}$. which for $n= 9$ and $r=30$ is $\abs{S}= (n+1) * (8*30^2)^(n+1) = \dots$. This is a large state space, but state of the art classical planners can deal with a large state space, as they can often search for plans by considering just a fraction of the states. Also, many of these states will not be reachable, because either the objects overlap between them, collide with static elements like walls, or some robot configurations can not be reached in order to grasp specific objects. Moreover, generalized planning allows to find a set of high-level actions useful for guiding the task and motion search problem, and serialized goals can deal with a greater number of movable objects. The domain encoding in STRIPS is shown in \ref{fig:modeling}. We describe the STRIPS encoding and report the results using generalized planning and comparing them with the results obtained by running STRIPS solvers on the whole problem, without generalization or serialization. It is important to remark that to encode our domains we ground the action arguments to each of their possible values. Then basic STRIPS ground actions become:

\smallskip

\begin{tabular}{l}
\footnotesize \code{rotate\_with\_obj}\texttt{(c1r, c2r, c1o, c2o, o, d, p1, p2)} \\
  \emph{pre:} \texttt{hand(o), conf(r,c1r), conf(o,c1o)}, clear(p1)\\
  \emph{add:} \texttt{conf(r,c2r), conf(o,c2o), clear(p2)}\\
  \emph{del:} \texttt{conf(r,c1r), conf(o,c1o), clear(p1)}
% \end{alltt}
\end{tabular}

\smallskip

\noindent where $o$ is an object, $d$ is an angular direction (clockwise, counter-clockwise), 
the $c$'s are  configurations, $p1$ is a set of cells which must be clear and $p2$ are the cells will not be clear after the action.

\\

\begin{figure*}[!htb]
  \centering
  \subcaptionbox{}[.45\linewidth][c]{%
    \hspace*{\fill}\includegraphics[height=4.5cm,width=6cm]{img/mgo_init2.png}}\
  \subcaptionbox{}[.45\linewidth][c]{%
    \hspace*{\fill}\includegraphics[height=4.5cm,width=6cm]{img/mgo_goal2.png}}\
   
\caption{Initial and target states for  \textit{MOVING-GEOM-OBSTACLE} instance  in which robot has to reach a target configuration, and  objects 
need  to be grasped and moved out of the way.}
\label{fig:images}
\end{figure*}



We define a CTMP problem as a tuple $P_{CTMP}=\langle  F_{CTMP},A_{CTMP},I_{CTMP},G_{CTMP}\rangle$ where $F_{CMTP}$ is a set of fluents which encode both, the symbolic and geometric information, and $A_{CTMP}$ stands for robot actions. For convenience, the encoding distinguish actions for translations and rotations with the object being held and without. $I_{CTMP} \subseteq F_{CTMP}$ provides the initial configuration of both, the robot and the movable objects, together with environment information, and $G_{CTMP} \subseteq F_{CTMP}$ is a goal.
\begin{itemize}


\item The main fluents $F_{CTMP}$ in the problem represent the configurations \emph{conf(r, c)} and \emph{conf(o, c)} for the robot $r$, the different movable objects $o$, and c's are configurations. The status of the gripper is represented by the fluent \emph{handempty}, which is true if the gripper is empty and false if the gripper is holding any object. The object $o$ being held is captured by \emph{hand(o)}. For efficiency purposes all configurations and actions are precompiled, so all of them are valid ones. The static world elements are known and configurations and actions are compiled based on this. So any robot or object configuration is collision free. For movable objects is different. We need some constraints to avoid collisions between them. For that purpose, since the geometric properties of the objects are known, we compute the set of possible overlaps between configurations, this set is added as preconditions and effects for all the (grounded) actions where the robot or an object would end up occupying a zone, based on the resolution parameter, after the action. To avoid overlappings we use a fluent of the form $clear(cell)$. An atom $clear(cell)$ in an action precondition must be true to allow a robot or an object to move from one configuration to another one. This means that the resulting configuration must not overlap any occupied space. On the same way, an atom $clear(cell)$ is included in the add list of actions to make $cell$ clear after a the action. Clear predicates are specified by cells and not by configurations because the conjunction of \emph{clear} atoms would be greater using configurations, since and object can be in $r_a$ possible orientations. 

\item Regarding $A_{CTMP}$: The grasp and place actions just change the state of the gripper, and for the robot to grasp an object, the gripper must be empty and the tip of the gripper must be sufficiently close to the object and with a correct orientation respect its center of mass, something that is captured by the fluent $\emph{graspable(rconf, oconf)}$ which represent that a robot being in configuration $rconf$ can grasp an object being at configuration $oconf$.
Given the discretization and the configuration space, the \emph{translate} and \emph{rotate} actions change the configuration of the robot translating or rotating it from one configuration to another one, taking as reference the center of mass of the robot. There are 8 possible $d$ directions and each one moves the robot to the corresponding neighboring cell in the $r \times r$ grid, by adding +1, 0, or -1 to the corresponding $x$ and $y$ coordinates. For the \emph{rotate} action, there are two possible $adir$ directions: \emph{clockwise} and \emph{counter-clockwise}, which add or subtract 45 degrees to $\theta$.  Actions \emph{translate-with-obj} and \emph{rotate-with-obj} are similar but update the configuration of both, the robot and the object being held. Moreover, since   graspable(cr, co) must be true when hand(o) becomes true, and remains true as long as the object $o$ is being held, the arguments for these actions must satisfy the graspable predicate. This is important, as the number of ground actions \emph{translate-with-obj} and \emph{rotate-with-obj}  ends up being linear in the total number of configurations instead of quadratic.

\item $I_{CTMP}$ encode the initial configurations of robot and objects, the initial status of the gripper and which discretized cells are clear.

\item $G_{CTMP}$ encode the goal in the form of what should be the final robot configuration of the robot or even the objects.

\end{itemize}


\begin{figure}[H]
\footnotesize
\centering
\begin{alltt}

\emph{types}: 
	robot, obj, conf, dir adir, pos

\emph{predicates}:
  translation(t: thing, c1, c2: conf, d:dir)
  rotation(c1, c2: conf, d: adir)
  orotation(o: obj, c1, c2: conf, d: adir)
  conf(r : robot, c: conf)
  conf(o : object, c: conf)
  graspable(c1, c2: conf)
  clear(p : pos)
  handempty
  hand(o : obj)

\code{translate}(c1, c2: conf, d: dir)
  \emph{prec:} handempty, conf(r, c1), 
        translation(r, c1, c2, d)
  \emph{add:} conf(r,c2)
  \emph{del:} conf(r,c1)

\code{rotate}(c1, c2: conf, d: adir) 
  \emph{prec:} handempty, conf(r, c1), rotation(c1, c2, d)
  \emph{add:} conf(r,c2)
  \emph{del:} conf(r,c1)

\code{translate-with-obj}(c1r,c2r,c1o,c2o:conf,o:obj,d:dir)
  \emph{prec:} hand(o), conf(r, c1r), conf(o, c1o), 
        translation(r, c1r, c2r, d), 
        translation(o, c1o, c2o, d)
  \emph{add:} conf(r,c2r), conf(o, c2o)
  \emph{del:} conf(r,c1r), conf(o, c1o)

\code{rotate\_with\_obj}(c1r,c2r,c1o,c2o:conf,o:obj,d:adir) 
  \emph{prec:} hand(o), conf(r, c1r), conf(o, c1o),
        rotation(c1r, c2r, d), 
        orotation(o, c1o, c2o, d )
  \emph{add:} conf(r,c2r), conf(o, c2o)
  \emph{del:} conf(r,c1r), conf(o, c1o)

grasp(o: obj, c1, c2: conf)
  \emph{prec:} graspable(c1,c2),conf(r,c1),
        conf(o,c2),handempty
  \emph{add:} hand(o)
  \emph{del:} handempty

place(o: obj)
  \emph{prec:} hand(o)
  \emph{add:} handempty
  \emph{del:} hand(o)

\end{alltt}
%\vskip -.3cm
\caption{\small Model in STRIPS. Our encoding ground arguments.}
\label{fig:modeling}
\end{figure}

