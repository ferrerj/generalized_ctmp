1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 6827
parse actions...
Number of actions: 156780
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 5300
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
4089 variables of 6827 necessary
0 of 0 mutex groups necessary.
156777 of 156780 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 8178
Preprocessor derived variables: 0
Preprocessor task size: 5248551
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=19.88s]
building causal graph...done! [t=24.92s]
packing state variables...done! [t=24.94s]
Variables: 4089
Facts: 8178
Bytes per state: 512
done initalizing global data [t=24.94s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 2426502 unary operators... done! [1349552 unary operators]
Best heuristic value: 29 [g=0, 1 evaluated, 0 expanded, t=26.76s, 1085980 KB]
Best heuristic value: 24 [g=1, 2 evaluated, 1 expanded, t=26.94s, 1085980 KB]
Best heuristic value: 22 [g=2, 3 evaluated, 2 expanded, t=27.12s, 1085980 KB]
Best heuristic value: 20 [g=4, 8 evaluated, 7 expanded, t=28.06s, 1085980 KB]
Best heuristic value: 18 [g=7, 14 evaluated, 13 expanded, t=29.20s, 1085980 KB]
Best heuristic value: 14 [g=13, 38 evaluated, 37 expanded, t=33.80s, 1085980 KB]
Best heuristic value: 10 [g=22, 126 evaluated, 125 expanded, t=50.12s, 1085980 KB]
Best heuristic value: 9 [g=23, 127 evaluated, 126 expanded, t=50.30s, 1085980 KB]
Best heuristic value: 8 [g=30, 154 evaluated, 153 expanded, t=55.02s, 1085980 KB]
Best heuristic value: 7 [g=34, 201 evaluated, 200 expanded, t=63.38s, 1085980 KB]
Best heuristic value: 5 [g=38, 208 evaluated, 207 expanded, t=64.60s, 1085980 KB]
Best heuristic value: 4 [g=39, 209 evaluated, 208 expanded, t=64.76s, 1085980 KB]
Best heuristic value: 3 [g=40, 210 evaluated, 209 expanded, t=64.92s, 1085980 KB]
Best heuristic value: 2 [g=41, 211 evaluated, 210 expanded, t=65.06s, 1085980 KB]
Best heuristic value: 1 [g=42, 212 evaluated, 211 expanded, t=65.22s, 1085980 KB]
Solution found!
Actual search time: 40.30s [t=65.24s]
to-c806-c802-W-o2-c1221-c1220 (1)
to-c802-c798-W-o2-c1220-c1219 (1)
to-c798-c794-W-o2-c1219-c1218 (1)
p-o2 (1)
tr-c794-W-c790 (1)
rr-c790-CCW-c317 (1)
rr-c317-CCW-c789 (1)
tr-c789-N-c713 (1)
rr-c713-CW-c254 (1)
tr-c254-NE-c194 (1)
tr-c194-N-c141 (1)
tr-c141-SE-c197 (1)
g-c197-o1-c213 (1)
to-c197-c194-W-o1-c213-c210 (1)
ro-c194-c655-CCW-o1-c210-c1021 (1)
p-o1 (1)
rr-c655-CW-c194 (1)
rr-c194-CW-c656 (1)
tr-c656-S-c718 (1)
tr-c718-S-c794 (1)
g-c794-o2-c1218 (1)
to-c794-c718-N-o2-c1218-c1188 (1)
to-c718-c656-N-o2-c1188-c1158 (1)
p-o2 (1)
rr-c656-CCW-c194 (1)
rr-c194-CCW-c655 (1)
g-c655-o1-c1021 (1)
to-c655-c659-E-o1-c1021-c1022 (1)
p-o1 (1)
rr-c659-CW-c197 (1)
tr-c197-W-c194 (1)
rr-c194-CW-c656 (1)
g-c656-o2-c1158 (1)
to-c656-c660-E-o2-c1158-c393 (1)
ro-c660-c197-CCW-o2-c393-c213 (1)
dummy-goal-o2-action-2 (1)
p-o2 (1)
rr-c197-CCW-c659 (1)
g-c659-o1-c1022 (1)
to-c659-c663-E-o1-c1022-c1023 (1)
to-c663-c667-E-o1-c1023-c1024 (1)
to-c667-c729-S-o1-c1024-c1025 (1)
dummy-goal-o1-action-1 (1)
Plan length: 43 step(s).
Plan cost: 43
Initial state h value: 29.
Expanded 212 state(s).
Reopened 0 state(s).
Evaluated 213 state(s).
Evaluations: 213
Generated 1815 state(s).
Dead ends: 0 state(s).
Search time: 40.30s
Total time: 65.24s
Solution found.
Peak memory: 1085980 KB

Plans and cost: 

