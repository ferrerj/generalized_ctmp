1. Running translator
Second argument is a file name: use two translator arguments.
parse domain header...
parse requirements...
parse predicates...
Number of atoms: 1135
parse actions...
Number of actions: 6456
parse axioms...
parse end of domain file...
parse problem header...
parse initial state...
parse end of problem file...
Number of fluents: 693
Writing output.sas...

2. Running preprocessor
read and verify
read metric
read variables
read mutexes
init state
read goal
read operators
read axioms
Building causal graph...
The causal graph is not acyclic.
720 variables of 1135 necessary
0 of 0 mutex groups necessary.
6304 of 6456 operators necessary.
0 of 0 axiom rules necessary.
Building domain transition graphs...
solveable in poly time 0
Building successor generator...
Preprocessor facts: 1440
Preprocessor derived variables: 0
Preprocessor task size: 67736
Writing output...
done


3. Running search
reading input... [t=0.00s]
Simplifying transitions... done!
done reading input! [t=0.22s]
building causal graph...done! [t=0.24s]
packing state variables...done! [t=0.24s]
Variables: 720
Facts: 1440
Bytes per state: 92
done initalizing global data [t=0.24s]
Conducting lazy best first search, (real) bound = 2147483647
Initializing FF heuristic...
Initializing additive heuristic...
Simplifying 30721 unary operators... done! [22070 unary operators]
Best heuristic value: 2 [g=0, 1 evaluated, 0 expanded, t=0.30s, 14492 KB]
Best heuristic value: 1 [g=1, 2 evaluated, 1 expanded, t=0.30s, 14492 KB]
Solution found!
Actual search time: 0.06s [t=0.30s]
tr-c41-N-c36 (1)
dummy-action-36 (1)
Plan length: 2 step(s).
Plan cost: 2
Initial state h value: 2.
Expanded 2 state(s).
Reopened 0 state(s).
Evaluated 3 state(s).
Evaluations: 3
Generated 18 state(s).
Dead ends: 0 state(s).
Search time: 0.06s
Total time: 0.30s
Solution found.
Peak memory: 14492 KB

Plans and cost: 

